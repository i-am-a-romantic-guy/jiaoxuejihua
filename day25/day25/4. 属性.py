# class Bmi:
#
#     def __init__(self,name,height,weight):
#
#         self.name = name
#         self.height = height
#         self.weight = weight
#
#     def bmi(self):
#         return self.weight/self.height**2
#
# obj = Bmi('赵嘎', 1.83, 65)
# print(obj.bmi())
# 结果虽然实现了,但是逻辑上感觉不合理.bmi应该是类似于name,age,height,等名词,
# 但是你把它当做方法使用了.


# class Bmi:
#
#     def __init__(self,name,height,weight):
#
#         self.name = name
#         self.height = height
#         self.weight = weight
#
#     @property
#     def bmi(self):
#         return self.weight/self.height**2
#
# obj = Bmi('赵嘎', 1.83, 65)
# print(obj.bmi)
# property 将执行一个函数需要函数名()变换成直接函数名.
# 将动态方法 伪装 成了一个属性,虽然在代码级别上没有什么提升,但是让你看起来更合理.
# obj.bmi
# obj.bmi
# obj.bmi = 666
#
# del obj.bmi

# property 他是一个组合.

# class Foo:
#     @property
#     def bmi(self):
#         print('get的时候运行我啊')
#
#     @bmi.setter
#     def bmi(self,value):
#         print(value)
#         print('set的时候运行我啊')
#         # return 111  # 无法得到返回值
#
#     @bmi.deleter
#     def bmi(self):
#         print('delete的时候运行我啊')
#         # return 111  # 无法得到返回值
#
# obj = Foo()
# # obj.bmi
# obj.bmi = 666 # 操作命令 这个命令并不是改变bmi的值,而是执行被bmi.setter装饰器装饰的函数
# # obj.bmi(666)
# del obj.bmi
# 应用场景:
    # 1, 面试会考一些基本的调用,流程.
    # 2, 工作中如果遇到了一些类似于属性的方法名,可以让其伪装成属性.


# 设置属性的两种方式:
    # 1, 利用装饰器设置属性.
# class Foo:
#     @property
#     def bmi(self):
#         print('get的时候运行我啊')
#
#     @bmi.setter
#     def bmi(self,value):
#         print(value)
#         print('set的时候运行我啊')
#         # return 111  # 无法得到返回值
#
#     @bmi.deleter
#     def bmi(self):
#         print('delete的时候运行我啊')
#         # return 111  # 无法得到返回值

    # 2. 利用实例化对象的方式设置属性.

# class Foo:
#     def get_AAA(self):
#         print('get的时候运行我啊')
#
#     def set_AAA(self,value):
#         print('set的时候运行我啊')
#
#     def delete_AAA(self):
#         print('delete的时候运行我啊')
#
#     AAA = property(get_AAA,set_AAA,delete_AAA) #内置property三个参数与get,set,delete一一对应

# f1=Foo()
# f1.AAA
# f1.AAA='aaa'
# del f1.AAA

