# isinstance 判断的是对象与类的关系
class A:
    pass


class B(A):
    pass


obj = B()

# isinstance(a,b) 判断的是 a是否是b类 或者 b类派生类 实例化的对象.
# print(isinstance(obj, B))  # True
# print(isinstance(obj, A))  # True


# issubclass 类与类之间的关系

class A:
    pass

class B(A):
    pass

class C(B):
    pass

# issubclass(a,b) 判断的是 a类是否是b类 或者 b类派生类 的派生类.
# issubclass(a,b) 判断的是 a类是否是b类 子孙类.
# print(issubclass(B,A))
# print(issubclass(C,A))

