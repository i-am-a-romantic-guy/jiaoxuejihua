# 类的私有成员：

# 1. 私有类的属性
    # 在类的内部可以访问
# class A:
#
#     name = '李业'
#     __name = '钢哥'  # 私有类的属性
#
#     def func(self):
#         print(self.name)
#         print(self.__name)
# obj = A()
# obj.func()
#

    # 类的外部不能访问

# class A:
#     name = '李业'
#     __name = '钢哥'  # 私有类的属性
#
#     def func(self):
#         pass

# obj = A()
# print(obj.name)
# print(A.__name)
# print(obj.__name)

    # 类的派生类

# class A:
#     name = '李业'
#     __name = '钢哥'
#
# class B(A):
#     def func(self):
#         print(self.__name)
#
# obj = B()
# # print(obj.__name)
# obj.func()

# 私有对象属性: 只能在类的内部使用,不能再类外部以及派生类使用.

# class A:
#
#     def __init__(self,name,pwd):
#
#         self.name = name
#         self.__pwd = pwd
#
#     def md5(self):
#         self.__pwd = self.__pwd + '123'
#
# obj = A('李业', 'liyedsb')
# print(obj.__pwd)

# 私有类的方法

# class A:
#
#     def func(self):
#         self.__func()
#         print('in A func')
#
#     def __func(self):
#         print('in A __func')
#
# obj = A()
# obj.func()
# obj.__func()

# 私有成员来说: 当你遇到重要的数据,功能,(只允许本类使用的一些方法,数据)设置成私有成员.


# python所有的私有成员都是纸老虎,形同虚设.

# class A:
#
#     name = '李业'
#     __name = '钢哥'  # 私有类的属性
#
#     def __func(self):
#         print('in __func')
#
# print(A.__dict__)
# print(A._A__name)

# 类从加载时,只要遇到类中的私有成员,都会在私有成员前面加上_类名 .




