"""
5.传入函数中多个列表和字典,如何将每个列表的每个元素依次添加到函数的动态参数args里面？
如何将每个字典的所有键值对依次添加到kwargs里面？
"""
# lst = [1,2,3,4]
# lst1 = [1,2,3,4]
# dic = {"key":1,"key3":3}
# dic1 = {"key2":1,"key5":3}
#
# def func(*args,**kwargs):
#     print(args,kwargs)
# func(*lst,*lst1,**dic,**dic1)

"""
7.写函数,接收两个列表,将列表长度比较小的列表返回.
"""
# lst = [1,2,3,4,5]
# lst1 = [1,3,4,5]
#
# def func(a,b):
#     return b if len(a) > len(b) else a
# print(func(lst,lst1))

# def wrapper():
#     def inner():
#         print(666)
#     inner()
# wrapper()

