import socket

# 买电话
phone = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  # 默认基于TCP协议的socket

# 绑定电话卡
phone.bind(('192.168.14.198',8848))  # 绑定IP地址和端口

# 开机监听
phone.listen(5)
print(111)

# 等待连接
conn, addr = phone.accept() # 阻塞
print(conn,addr)

from_client_data = conn.recv(1024)  # 至多接受1024个字节  阻塞
print(f'来自客户端{addr[0]}的消息:{from_client_data.decode("utf-8")}')

to_client_data = input('>>>')
conn.send(to_client_data.encode('utf-8'))
conn.close()
phone.close()