from django.shortcuts import render,HttpResponse,redirect
from django import forms
from django.http import JsonResponse
from utils.md5_func import md5_function
# Create your views here.
from sales import models

def login(request):
    res_dict = {'status':None,'home':None,'msg':None}

    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_objs = models.UserInfo.objects.filter(username=username,password=md5_function(password))
        if user_objs:
            #{'status': 1, 'home': '/home/', 'msg': '用户名或者秘密错误'}
            res_dict['status'] = 1
            res_dict['home'] = '/home/'
            return JsonResponse(res_dict)

        else:
            res_dict['status'] = 0
            res_dict['msg'] = '用户名或者秘密错误'
            return JsonResponse(res_dict)

class RegisterForm(forms.Form):

    username = forms.CharField(

        max_length=16,
        min_length=4,
        error_messages={
            'max_length':'太长了',
            'min_length':'太短了,你不行',
            'required':'不能为空',
        },
        widget=forms.TextInput(attrs={'class':'username','placeholder':'您的用户名','autocomplete':'off'})
    )

    #<input type="text" name="username" class="username" placeholder="您的用户名" autocomplete="off">
    password = forms.CharField(
        max_length=32,
        error_messages={
            'required': '不能为空',
            'max_length': '不能太长',
        },
        widget=forms.PasswordInput(attrs={'class':'password','placeholder':'输入密码'
            ,'oncontextmenu':'return false','onpaste':'return false'}),

    )
#<input type="password" name="password" class="password" placeholder="输入密码" oncontextmenu="return false"
                   #onpaste="return false">
    confirm_password = forms.CharField(
        max_length=32,
        error_messages={
            'required': '不能为空',
            'max_length': '不能太长',
        },
        widget=forms.PasswordInput(attrs={'class': 'confirm_password', 'placeholder': '输入密码'
            , 'oncontextmenu': 'return false', 'onpaste': 'return false'}),
    )

    telephone = forms.CharField(
        max_length=11,
        min_length=11,
        error_messages={
            'max_length': '不能太长,需11位',
            'min_length': '不能太短,需11位',
            'required':'不能为空'
        },
        widget=forms.TextInput(attrs={'class':'phone_number','placeholder':'输入手机号码','autocomplete':'off','id':'number'})

    )
    #<input type="text" name="phone_number" class="phone_number" placeholder="输入手机号码" autocomplete="off"
                   # id="number">
    email = forms.EmailField(
        error_messages={
            'invalid': '必须是邮箱格式',
            'required': '不能为空'
        },
        widget=forms.EmailInput(
            attrs={ 'class': 'email', 'placeholder': '输入邮箱地址',
                   'oncontextmenu': 'return false','onpaste': 'return false'})

    )

    def clean(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password == confirm_password:
            return self.cleaned_data

        else:
            self.add_error('confirm_password','两次密码不一致!')
    #xx@xx
#<input type="email" name="email" class="email" placeholder="输入邮箱地址" oncontextmenu="return false"#}
#{#                   onpaste="return false">#}
    # def __init__(self):



def register(request):

    if request.method == 'GET':

        register_obj = RegisterForm()

        return render(request,'register.html',{'register_obj':register_obj})
    else:
        register_obj = RegisterForm(request.POST)
        if register_obj.is_valid():
            # print(register_obj.cleaned_data)
            register_obj.cleaned_data.pop('confirm_password')
            md5_password = md5_function(register_obj.cleaned_data.get('password'))
            register_obj.cleaned_data.update({'password':md5_password})
            models.UserInfo.objects.create(
                **register_obj.cleaned_data
            )

            return redirect('login')
        else:
            return render(request,'register.html',{'register_obj':register_obj})

def home(request):

    return HttpResponse('ojbk')

