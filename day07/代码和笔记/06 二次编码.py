# 密码本:
# ascii  -- 没有中文
# gbk    -- 英文 8b(位) 1B(字节) 中文 16b 2B
# unicode -- 英文16b 2B  中文32b 4B
# utf-8 --   英文8b 1B  欧洲16b 2B  亚洲24b 3B


# name = "你好啊"
# s1 = name.encode("utf-8") # 编码  9
# s2 = name.encode("gbk") # 编码  6
# s2 = s1.decode("utf-8") # 解码
# print(s2.encode("gbk"))

# 以什么编码集(密码本)进行编码就要用什么编码集(密码本)解码


