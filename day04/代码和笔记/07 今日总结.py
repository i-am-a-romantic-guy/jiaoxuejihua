# 列表 -- list
#     有序,可变的,索引,
#     作用:存储数据的,支持很多种数据类型

# 定义方式:
# lst = [1,"alex","黑哥"]

# 增:
# append 追加
# insert 插入
# extend 迭代添加

# 删
# del lst  -- 删除整个列表
# del lst[1:2] -- 切片删除
# del lst[1:4:2] -- 步长删除

# pop
#   1.默认删除最后一个
#   2.有返回值返回的是被删除的元素
#   3.通过索引进行删除

# clear 清空列表
# remove 通过元素进行删除

# 改
# lst[0] = 1
# lst[1:2] ="qaaa"   # 元素可以超出切片的位数
# lst[1:4:2] = "12"  # 元素和位置一一对应,多也不行少也不行

# 查
# for i in lst:
#     print(i)

# 列表的嵌套:
# 取值通过索引进行取值,一层一层的进行查找

# 2.元组 -- tuple
#   只能索引查看和for查看,不能进行增删改
#   存储一些重要的信息数据
#   元组是不可变的列表

# 3.range -- 范围
    # python3:
        # range 是一个可迭代对象
    # python2:
        #xrange和python3中的range是相似的
        # range返回一个列表
    # range 是顾头不顾尾

    # range(起始位置,终止位置)  --  [起始位置:终止位置]
    # range(终止位置)  --  [:终止位置]
    # range(起始位置,终止位置,步长)  --  [起始位置:终止位置:步长]

    # for 和 range配合使用

# 面试题:

# lst = []
# for i in lst:
#     lst.append("alex")
#     print(lst)  # 不会打印内容  因为lst是空的

# lst = [1,2]
# for i in lst:
#     lst.append("alex")
#     print(lst) # 循环打印lst中的内容 -- 此循环是死循环

# lst = [1,2]
# for i in lst:
#     lst.append("alex")
# print(lst)  # 死循环 -- 不会打印内容

# lst = []
# for i in range(5):
#     lst.append([])
# print(lst)  # [[],[],[],[],[]]

# lst = [1,2]
# lst[0] = lst
# print(lst)  # [[...],2]