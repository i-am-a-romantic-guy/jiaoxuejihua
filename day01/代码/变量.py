# 什么是变量?
    # 变量就是将一个临时的值存储在内容中(存储中间值/起别名)
# 变量能干什么?
    # 变量可以多次重复使用
# 变量在那用怎么用?
    # 变量在咱们程序中会频繁使用

# 常量在配置文件中声明使用

# print(35+56)   # 打印-输出
# print((35+56)*2)   # 打印-输出
# print(((35+56))*2)   # 打印-输出

# a = 35+56
# print(a)
# b = a*2
# print(b)
# print(1+a)

# a = "藿香正气水"  # 声明变量
# b = "藿香正气水"

# a # 变量的名字
# = # 赋值
# "藿香正气水" # 值

# 变量定义的规则:
# 1.变量由数字,字母,下划线组成
# 2.不能以数字开头
# 3.不能使用python中关键字
# 4.不能使用中文和拼音
# 5.区分大小写
# 6.变量名要具有描述性
# 7.推荐写法
#     7.1驼峰体
#     7.2下划线
# age_of_oldboy = 98  (官方推荐)

# age = 18
# age1 = 19
# age2 = age # age2 = 18
# age = 20
# # 20 19 18
# print(age,age1,age2)

# a = 4
# b = a + 6
# print(b)

# # 常量:
# ID = 11012013014066