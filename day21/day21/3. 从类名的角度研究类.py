# class Human:
#     """
#     类的具体结构
#     """
#     # 第一部分:静态属性
#     mind = '有思想'   # 类的属性  (静态属性, 静态字段)
#     language = '使用语言'
#
#     # 第二部分: 动态方法
#     def work(self):
#         print('人类都会工作')
#
#     def eat(self):
#         print('人类都需要吃饭')

# 1. 类名操作类中的属性
#     1. 类名查看类中所有的内容
# print(Human.__dict__)
#     2. 类名操作类中的静态属性  万能的点.

# 增:
# Human.body = '有头和四肢'
# 删:
# del Human.mind
# 改:
# Human.mind = 'liye脑残'
# 查:
# print(Human.language)
# print(Human.__dict__)

# 2. 类名调用类中的方法(一般类中的(静态方法,类方法)方法不会通过类名调用)
# Human.work(111)

# 总结:
# 一般类名就是操作类中的属性.
