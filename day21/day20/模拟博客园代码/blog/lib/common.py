from core import src

def auth(func):
    def inner(*args, **kwargs):
        if src.status_dic['status']:
            ret = func(*args, **kwargs)
            return ret
        else:
            print('\033[1;31;0m 请先进行登录 \033[0m')
            if src.login():
                ret = func(*args, **kwargs)
                return ret

    return inner


def get_logger():
    path = r'F:\s24\day21\liye.log'
    LOGGING_DIC['handlers']['file']['filename'] = path
    logging.config.dictConfig(LOGGING_DIC)  # 导入上面定义的logging配置
    logger = logging.getLogger()  # 生成一个log实例
    return logger
