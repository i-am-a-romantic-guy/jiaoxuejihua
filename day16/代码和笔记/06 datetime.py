from datetime import datetime,timedelta

# datetime -- 对象
# print(type(datetime.now()))

# print(datetime.now()) # 获取当前时间
# print(datetime(2019,5,20,15,14,00) - datetime(2019,5,20,14,20,00))

# 将当前时间转化成时间戳
# t = datetime.now()
# print(t.timestamp())

# 将时间戳转化成当前时间
# import time
# print(datetime.fromtimestamp(15000000000))

# 将字符串转成对象
# print(type(datetime.strptime("2019-10-10 22:23:24","%Y-%m-%d %H:%M:%S")))

# 将对象转成字符串
# print(str(datetime.now()))
# print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


# datetime加减
# print(datetime.now() + timedelta(hours=30 * 24 * 12))
# print(datetime.now() - timedelta(hours=30 * 24 * 12))


# 总结:time
#     用处: 记录日志时使用
#     计算时间