import random

# print(random.random())          # 0 ~ 1
# print(random.uniform(1,10))     # 1 ~ 10
# print(random.randint(1,50))     # 1 ~ 50(闭区间)
# print(random.randrange(1,5,2))    # randrange(起始,终止,步长)
# print(random.choice([1,2,3,4,5,])) # 选择一个元素
# print(random.choices([1,2,3,4,5,],k=2))   # 选择两个元素,会有重复
# print(random.sample((1,2,3,4,5),k=2))  # 选择两个元素,不会有重复(除非只有两个)

# lst = [1,2,3,4,5,6,7,8,9,0]
# random.shuffle(lst)  # 顺序打乱
# print(lst)

#练习题:验证码
