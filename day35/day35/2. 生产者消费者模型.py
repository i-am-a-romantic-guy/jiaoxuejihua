# from multiprocessing import Process
# from multiprocessing import Queue
# import time
# import random
#
# def producer(q,name):
#     for i in range(1,6):
#         time.sleep(random.randint(1,2))
#         res = f'{i}号包子'
#         q.put(res)
#         print(f'生产者{name} 生产了{res}')
#
#
# def consumer(q,name):
#
#     while 1:
#         try:
#             food = q.get(timeout=3)
#             time.sleep(random.randint(1, 3))
#             print(f'\033[31;0m消费者{name} 吃了{food}\033[0m')
#         except Exception:
#             return
#
#
# if __name__ == '__main__':
#     q = Queue()
#
#     p1 = Process(target=producer,args=(q,'孙宇'))
#     p2 = Process(target=consumer,args=(q,'海狗'))
#
#     p1.start()
#     p2.start()