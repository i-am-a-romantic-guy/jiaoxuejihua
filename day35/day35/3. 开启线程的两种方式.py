# 第一种方式
# from threading import Thread
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(1)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#
#     t1 = Thread(target=task,args=('海狗',))
#     t1.start()
#     print('===主线程')  # 线程是没有主次之分的.



# 第二种方式:

# from threading import Thread
# import time
#
# class MyThread(Thread):
#
#     def __init__(self,name,l1,s1):
#         super().__init__()
#         self.name = name
#         self.l1 = l1
#         self.s1 = s1
#     def run(self):
#         print(f'{self.name} is running')
#         time.sleep(1)
#         print(f'{self.name} is gone')
#
# if __name__ == '__main__':
#     t1 = MyThread('李业', [1,2,3], '180')
#     t1.start()
#     print('=====主线程')