# 守护进程:
# 古时候 太监守护这个皇帝,如果皇帝驾崩了,太监直接也就死了.
# 子进程守护着主进程,只要主进程结束,子进程跟着就结束,


# from multiprocessing import Process
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(2)
#     print(f'{name} is gone')
#
#
#
# if __name__ == '__main__':
#     # 在windows环境下, 开启进程必须在 __name__ == '__main__' 下面
#     p = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p.daemon = True  # 将p子进程设置成守护进程,只要主进程结束,守护进程马上结束.
#     p.start()
#     # p.daemon = True  # 一定要在子进程开启之前设置
#     time.sleep(1)
#     print('===主')