# 开启进程的第一种方式:
# from multiprocessing import Process
# import time
#
# def task(name):
#     print(f'{name} is running')
#     time.sleep(2)
#     print(f'{name} is gone')
#


# if __name__ == '__main__':
#     # 在windows环境下, 开启进程必须在 __name__ == '__main__' 下面
#     p = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     p.start()  # 只是向操作系统发出一个开辟子进程的信号,然后就执行下一行了.
    # 这个信号操作系统接收到之后,会从内存中开辟一个子进程空间,然后在将主进程所有数据copy加载到子进程,然后在调用cpu去执行.
    # 开辟子进程开销是很大的.
    # print('==主开始')
    # time.sleep(3)


    # 所以永远会先执行主进程的代码.



# 开启进程的第二种方式:

# from multiprocessing import Process
# import time
#
# class MyProcess(Process):
#
#     def __init__(self,name):
#         super().__init__()
#         self.name = name
#
#     def run1(self):
#         print(f'{self.name} is running')
#         time.sleep(2)
#         print(f'{self.name} is gone')
#
#
# if __name__ == '__main__':
#     p = MyProcess('常鑫')
#     p.start()
#     print('===主')


# 简单应用:


# from multiprocessing import Process
# import time
# #
# def task(name):
#     print(f'{name} is running')
#     time.sleep(1)
#     print(f'{name} is gone')
#
# def task1(name):
#     print(f'{name} is running')
#     time.sleep(2)
#     print(f'{name} is gone')
#
# def task2(name):
#     print(f'{name} is running')
#     time.sleep(3)
#     print(f'{name} is gone')
#
#
# if __name__ == '__main__':
#     # 在windows环境下, 开启进程必须在 __name__ == '__main__' 下面
#     # p1 = Process(target=task,args=('常鑫',))  # 创建一个进程对象
#     # p2 = Process(target=task,args=('李远点业',))  # 创建一个进程对象
#     # 一个进程串行的执行三个任务
#     start_time = time.time()
#     task('常鑫')
#     task1('李远点业')
#     task2('海狗')
#     print(f'结束时间{time.time() - start_time}')
    # 三个进程 并发或者并行的执行三个任务
    # start_time = time.time()
    # p1 = Process(target=task, args=('常鑫',))  # 创建一个进程对象
    # p2 = Process(target=task1, args=('李远点业',))  # 创建一个进程对象
    # p1.start()
    # p2.start()
    # task2('海狗')
    # print(f'结束时间{time.time()-start_time}')