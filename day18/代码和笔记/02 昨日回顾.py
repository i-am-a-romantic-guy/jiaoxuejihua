# 1.序列化:
#     json:
#         dump  序列化(将数据类型对象转换成字符串,写入文件中)
#         load  反序列化(将文件中的字符串转换成数据类型对象)
#         dumps (将对象转换成字符串)
#         loads (将字符串转换成对象)
#     pickle:
#         dump  序列化(将数据类型对象转换成类似于字节,写入文件中)
#         load  反序列化(将文件中的类似字节转换成数据类型对象)
#         dumps (将对象转换成类似字节)
#         loads (将类似字节转换成对象)

# 2.os -- 操作系统:
#     1.目录
#         os.getcwd()
#         os.chdir()
#     2.文件夹
#         mkdir() # 创建一个文件夹
#         rmdir() # 删除一个文件夹
#         makedirs() # 递归创建文件夹
#         removedirs() #递归删除文件夹
#         listdir()
#     3.文件
#         os.rename()
#         os.remove()
#     4.路径
#         os.path.abspath() 绝对路径
#         os.path.split()  # 路径分割
#         os.path.join()   # 路径拼接
#         os.path.basename() # 路径中的文件名
#         os.path.dirname() # 没有文件名的路径
#         os.path.isdir()   # 判断是不是路径
#         os.path.isabs()   # 判断是不是绝对路径
#         os.path.isfile()  # 判断文件存不存在
#         os.path.exists()  # 判断路径是不是存在
#         os.path.getsize() # 获取文件大小

# sys -- python解释器
    # sys.exit()
    # sys.version
    # sys.path()
    # sys.platform
    # sys.argv

# hashlib -- 加密/校验
# md5,sha1,sha256,sha512

# 明文 -- 字节 -- 密文

# import hashlib
# md5 = hashlib.md5()
# md5.update("宝元".encode("utf-8"))
# print(md5.hexdigest())

# 加固定盐
# import hashlib
# md5 = hashlib.md5(b"alex")
# md5.update("宝元".encode("utf-8"))
# print(md5.hexdigest())

# collections
# 1.命名元组:
# 2.默认字典值:
# 3.双端队列:
# 4.计数器:   ***

# from collections import Counter
# s = "asdfaasdfas"
# print(dict(Counter(s)))