# 1.
# 暴力摩托程序（完成下列需求）：
#
# 1.
# 创建三个游戏人物，分别是：
#
# ​    苍井井，女，18，攻击力ad为20，血量200
#
# ​    东尼木木，男，20，攻击力ad为30，血量150
#
# ​    波多多，女，19，攻击力ad为50，血量80
#
# 2.
# 创建三个游戏武器，分别是：
#
# ​    平底锅，ad为20
#
# ​    斧子，ad为50
#
# ​    双节棍，ad为65
#
# 3.
# 创建三个游戏摩托车，分别是：
#
# ​                小踏板，速度60迈
#
# ​                雅马哈，速度80迈
#
# ​                宝马，速度120迈。
#
# ​    完成下列需求（利用武器打人掉的血量为武器的ad + 人的ad）：

class Jacked:  # 人物


    def __init__(self, name, sex, age, ad, hp):
        self.name = name
        self.sex = sex
        self.age = age
        self.ad = ad
        self.hp = hp

    def jacked(self, name):  # 徒手攻击
        name.hp -= self.ad
        print(f'{self.name}赤手空拳攻击了{name.name},{name.name}还剩{name.hp}血')

    def weapon(self, wea):  # 武器
        self.wea = wea

    def motorcycle(self, moto):  # 车  依赖
        self.moto = moto  # 组合


class Weapon:  # 武器
    def __init__(self, weapon_name, ad):
        self.weapon_name = weapon_name
        self.ad = ad

    def jacked(self, name, name_):
        name_.hp = name_.hp - name.ad - self.ad
        print(f'{name.name}用{self.weapon_name}攻击了{name_.name},{name_.name}还剩{name.hp}血')

    def jacked_(self, name, name_):
        name_.hp = name_.hp - name.ad - name.name_1.ad
        print(
            f'{name.name}骑着{name.name_2.motorcycle_name}打了骑着{name_.name_2.motorcycle_name}的{name_.name}一{name.name_1.weapon_name}，{name_.name}哭了')


class Motorcycle:  # 摩托
    def __init__(self, motorcycle_name, speed):
        self.motorcycle_name = motorcycle_name
        self.speed = speed

    def motorcycle_(self, name):  # 开摩托
        print(f'{name.name}骑着{self.motorcycle_name}开着{self.speed}迈的速度行驶在赛道上')



name_P1 = Jacked('苍井井', '女', '18', 20, 200)
name_P2 = Jacked('东尼木木', '男', '20', 30, 150)
name_P3 = Jacked('波多多', '女', '19', 50, 80)

name_P11 = Weapon('平底锅', 20)
name_P12 = Weapon('斧子', 50)
name_P13 = Weapon('双节棍', 60)

name_P111 = Motorcycle('小踏板', "60")
name_P112 = Motorcycle('雅马哈', "80")
name_P113 = Motorcycle('宝马', "120")

# ​    （1）苍井井骑着小踏板开着60迈的车行驶在赛道上。
name_P1.motorcycle(name_P111)
name_P1.moto.motorcycle_(name_P1)

# ​    （2）东尼木木骑着宝马开着120迈的车行驶在赛道上。
#
# ​    （3）波多多骑着雅马哈开着80迈的车行驶在赛道上。
#
# ​    （4）苍井井赤手空拳打了波多多20滴血，波多多还剩xx血。
#
# ​    （5）东尼木木赤手空拳打了波多多30滴血，波多多还剩xx血。
#
# ​    （6）波多多利用平底锅打了苍井井一平底锅，苍井井还剩xx血。
#
# ​    （7）波多多利用斧子打了东尼木木一斧子，东尼木木还剩xx血。
#
# ​    （8）苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血。（选做）
#
# ​    （9）波多多骑着小踏板打了骑着雅马哈的东尼木木一斧子，东尼木木哭了，还剩xx血。（选做）
#
# 2.
# 定义一个类，计算圆的周长和面积。
#
# 3.
# 定义一个圆环类，计算圆环的周长和面积（升级题）。
#
# 4.
# 定义一个学校类，一个老师类。
#
# - 学校类要求：
#
# - 学校类封装学校名称，学校地址，以及相关老师（以列表形式存放老师对象）的属性。
# - name: 学校名称。
# - address: 具体地址。
# - teacher_list: []。

# class School:
#
#     def __init__(self, name, address,teacher_list=[]):
#
#         self.name = name
#         self.address = address
#         self.teacher_list = teacher_list
#
#     def add_teacher_object(self, obj):
#         self.teacher_list.append(obj)

# - 学校类设置添加老师对象的方法。
#
# - 老师类封装姓名，教授学科，以及所属学校的具体对象。
#
# - name: 老师名。
# - course: 学科。
# - school: 具体学校对象。

# class Teacher:
#
#     def __init__(self,name,course,school_object):
#         self.name = name
#         self.course = course
#         self.school_object = school_object



# - 实例化2个校区：
#
# - 北京校区，美丽的沙河；
# - 深圳校区，南山区。
#
# - 实例化6个老师：
#
# - 太白，Python, 北京校区对象。
# - 吴超，linux, 北京校区对象。
# - 宝元，python, 北京校区对象。
# - 苑昊，python, 深圳校区对象。
# - 小虎，linux, 深圳校区对象。
# - 小王，Python，深圳校区对象。

class School:

    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.teacher_list = []

    def add_teacher_object(self, obj):
        self.teacher_list.append(obj)


class Teacher:

    def __init__(self, name, course, school_object):
        self.name = name
        self.course = course
        self.school_object = school_object

school1 = School('北京校区', '美丽的沙河')
school2 = School('深圳校区', '南山区')

taibai = Teacher("太白", "Python", school1)
wuchao = Teacher("吴超", "linux", school1)
baoyuan = Teacher("宝元", "Python", school1)
yuanhao = Teacher("苑昊", "Python", school2)
xiaohu = Teacher("小虎", "linux", school2)
xiaowang = Teacher("小王", "Python", school2)



# - 完成以下具体需求：
#
# 1.
# 获取太白所属学校名称。
# print(taibai.school_object.name)
# 2.
# 获取太白所属学校的学校地址。
# print(taibai.school_object.address)
# 3.
# 将所有属于北京校区的所有老师名展示出来，并添加到一个列表中。
# 1. 先要将属于北京校区校区的所有老师对象添加到北京校区对象的teacher_list
# school1.add_teacher_object(taibai)
# school1.add_teacher_object(wuchao)
# school1.add_teacher_object(baoyuan)
# print(school1.teacher_list)
# 2. 循环输入老师名,添加到一个列表中
# school1_teacher = []
# for teacher in school1.teacher_list:
#     print(teacher.name)
#     school1_teacher.append(teacher.name)
# print(school1_teacher)
# 4.
# 将所有属于深圳校区的所有老师以及所负责的学科展示出来。
# school2.add_teacher_object(yuanhao)
# school2.add_teacher_object(xiaohu)
# school2.add_teacher_object(xiaowang)
# for i in school2.teacher_list:
#     print(f'老师姓名:{i.name} 负责学科:{i.course}')
# 5.
# 将两个校区的负责Python学科的所有老师对象添加到一个列表中。



# 6.
# 将两个校区的负责linux学科的所有老师对象添加到一个列表中并循环展示出来。
# 7.
# 将北京校区这个对象利用pickle写入文件中，然后读取出来，并展示出所属于北京校区的老师姓名
# import pickle
# # with open('test1',mode='wb') as f1:
# #     pickle.dump(school1,f1)
#
with open('test1',mode='rb') as f1:
    obj = pickle.load(f1)
print(obj)
print(obj.name)

# 思考题:
# 同一个文件如果存储多个对象,如何取出多个对象?
#
#
