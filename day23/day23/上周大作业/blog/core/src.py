from conf import settings
from lib import common
import os
status_dic = {
    'username': None,
    'status': False,
}
flag = True

def login():
    i = 0
    with open(settings.REGISTER_PATH, encoding='utf-8') as f1:
        dic = {i.strip().split('|')[0]: i.strip().split('|')[1] for i in f1}
    while i < 3:
        username = input('请输入用户名：').strip()
        password = input('请输入密码：').strip()
        if username in dic and dic[username] == password:
            print('登录成功')
            status_dic['username'] = username
            status_dic['status'] = True
            return True
        else:
            print('用户名密码错误，请重新登录')
            i += 1


def register():
    with open(settings.REGISTER_PATH, encoding='utf-8') as f1:
        dic = {i.strip().split('|')[0]: i.strip().split('|')[1] for i in f1}
    while 1:
        print('\033[1;45m 欢迎来到注册页面 \033[0m')
        username = input('请输入用户名：').strip()
        if not username.isalnum():
            print('\033[1;31;0m 用户名有非法字符，请重新输入 \033[0m')
            continue
        if username in dic:
            print('\033[1;31;0m 用户名已经存在，请重新输入 \033[0m')
            continue
        password = input('请输入密码：').strip()
        if 6 <= len(password) <= 14:
            with open(settings.REGISTER_PATH, encoding='utf-8', mode='a') as f1:
                f1.write(f'\n{username}|{password}')
            status_dic['username'] = str(username)
            status_dic['status'] = True
            print('\033[1;32;0m 恭喜您，注册成功！已帮您成功登录~ \033[0m')
            return True
        else:
            print('\033[1;31;0m 密码长度超出范围，请重新输入 \033[0m')


@common.auth
def check_balance():
    """
       此函数是完成查询余额功能
       通过用户已登录的用户名获取用户的文件从而获取用户的余额。
    """
    # 打印日志
    log_path = os.path.join(settings.LOG_PATH,f"{status_dic['username']}.log")
    logger = common.get_logger(log_path)
    logger.info(f'{status_dic["username"]}查询了余额')





@common.auth
def save():
    """
       此函数是完成存款功能
       将用户输入的钱数存入到对应的用户文件中。
    """
    log_path = os.path.join(settings.LOG_PATH, f"{status_dic['username']}.log")
    logger = common.get_logger(log_path)
    logger.info(f'{status_dic["username"]}完成了存钱功能')


@common.auth
def transfer():
    pass


@common.auth
def bill_flow():
    """
    此函数用户查询流水：打开自己Log文件查询。
    :return:
    """
    pass


def login_out():
    pass

menu_dict = {
    1: login,
    2: register,
    3: check_balance,
    4: save,
    5: transfer,
    6: bill_flow,
    7: login_out,
}


def run():
    while True:
        print('''\033[1;32;0m
        欢迎访问ATM智能取款购物一体机
        1，登录
        2，注册
        3，查看余额
        4，存钱
        5，转账
        6，查看最近流水
        7，购物
        8，查看已购买商品
        9，查看心仪商品
        10，退出
             \033[0m''')
        num = input('\033[1;37;40m请输入你要办理的业务：\033[0m').strip()
        if num.isdigit():
            num = int(num)
            if 0 < num <= len(menu_dict):
                # 根据用户输入的数字选项，执行menu_dict字典对应的函数
                menu_dict[num]()

            else:
                print('\033[1;31;0m输入超出范围，请重新输入\033[0m')

        else:
            print('\033[1;31;0m重新输入...\033[0m')