# __item__系列
# __getitem__  __setitem__  __delitem__  对对象做类似于字典的(增删改查)触发__item__系列
# __delattr__ del obj.属性  就会触发此方法
# class Foo:
#     def __init__(self,name):
#         self.name=name
    #
    # def __getitem__(self, item):
    #     # print(item)
    #     # print(666)
    #     return self.__dict__[item]

    # def __setitem__(self, key, value):
    #     # self.__dict__[key]=value
    #     print(key)
    #     print(value)


    # def __delitem__(self, key):
    #     print('del obj[key]时,我执行')


    #
#     def __delattr__(self, item):
#         super().__delattr__(item)
#         print(f'对象的{item}属性已经删除')
#
#
# f1=Foo('sb')
# print(f1['name'])
# f1[1] = 2
# f1['age']
# del f1[1]

# del f1.name
# print(f1.name)

# f1['age']=18
# f1['age1']=19
# del f1.age1
# del f1['age']
# f1['name']='alex'
# print(f1.__dict__)


# __enter__ __exit__  with 上下文管理

# class A:
#
#     def __init__(self,name):
#         self.name = name
#
#     def __enter__(self):
#         print(666)
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print(777)
# # obj = A('海狗')
# # 实例化对象的第二种方式: 必须基于 __enter__ 以及 __exit__这个两个方法.
# with A('海狗') as obj:
#     print(obj.name)



# class A:
#
#     def __init__(self, text):
#         self.text = text
#
#     def __enter__(self):  # 开启上下文管理器对象时触发此方法
#         self.text = self.text + '您来啦'  # 第一步
#         print(11111)
#         return self  # 必须!!!将实例化的对象返回f1
#
#     def __exit__(self, exc_type, exc_val, exc_tb):  # 执行完上下文管理器对象f1时触发此方法
#         print(333)  # 第三步
#         self.text = self.text + ',这就走啦'
#
#
# with A('大爷') as f1:
#     print(2222)
#     print(f1.text)  # 第二步
# print(f1.text)  # 第四步



# __iter__

# class A:
#
#     def __init__(self,name):
#         self.name = name
#
#     def __iter__(self):
#         for i in range(10):
#             yield i
#
#
#     # def __next__(self):
#     #     pass
# obj = A('李业')  # obj 一个可迭代对象
# # print('__iter__' in dir(obj))
# # for i in obj:
# #     print(i)
#
# # print(obj.name)
# o = iter(obj)
# print(next(o))
# print(next(o))
# print(next(o))
# print(next(o))
# print(next(o))
# print(next(o))


# class B:
#     print(666)
#     country= '中国'
#
#
#
# class A(B):
#     print(777)
#     area = '河北'
#
#     def func(self):
#         print('in A func')
#
type
# print(888)














