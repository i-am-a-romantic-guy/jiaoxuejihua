

# 昨日内容回顾



#### null和undefined

```
null表示值是空，一般在需要指定或清空一个变量时才会使用，如 name=null;

undefined表示当声明一个变量但未初始化时，该变量的默认值是undefined。还有就是函数无明确的返回值时，返回的也是undefined。
	var a; -- undefined
	
null表示变量的值是空，undefined则表示只声明了变量，但还没有赋值。
```



#### object类型

```
var a = 'xx';
var b = new String('oo');
```

##### 数组

```
var a = [11,22,33];
typeof a; -- "object"

var b = new Array([11,22,33,44]);
typeof b; -- "object"
```

###### 数组常用方法

```
var a = [11,22,33];
索引取值 -- a[0];
数组长度 -- a.length;
尾部追加 -- a.push(44);
尾部删除 -- a.pop()
	示例:
		var a = [11, 22, 33, 44];
		var b = a.pop();
		结果:
            a -- [11, 22, 33]
            b -- 44
头部添加 -- a.unshift('aa')
	示例:
		 var a = [11, 22, 33];
		 a.unshift('aa')
		 a --  ["aa", 11, 22, 33]
头部删除 -- shift()
	示例:
		var a = ["aa", 11, 22, 33];
		a.shift() -- 'aa'
		a -- [11, 22, 33];

切片 -- slice()
	var b = a.slice(0,3);
	b -- [11, 22]
反转 reverse()
	var a = [11,22,33];
	a.reverse() 
	a -- [33,22,11]

数组元素拼接 join
	示例:
		var a = ['aa','bb','cc'];
		var b = a.join('_');
		b -- "aa_bb_cc";

数组合并 concat
	var a = ["aa", "bb", "cc"];
	var b = [11,22];
	var c = a.concat(b);
	c -- ["aa", "bb", "cc", 11, 22];
排序 sort 比较尬
	示例:
		var a = [12,3,25,43];
		对a进行升序排列:
		1 定义函数
			function sortNumber(a,b){
                return a - b
             };
		2 var b = a.sort(sortNumber)
		b -- [3, 12, 25, 43]
	sort 规则:
		  如果想按照其他标准进行排序，就需要提供比较函数，也就是自己提供一个函数提供排序规则，该函数要比较两个值，然后返回一个用于说明这两个值的相对顺序的数字。比较函数应该具有两个参数 a 和 b，其返回值如下：
　　　　　　若 a 小于 b，在排序后的数组中 a 应该出现在 b 之前，则返回一个小于 0 的值。
　　　　　　若 a 等于 b，则返回 0。
　　　　　　若 a 大于 b，则返回一个大于 0 的值。

删除 .splice() 
	示例:
		var a = ['aa','bb',33,44];
		单纯删除:a.splice(1,1)
		a -- ["aa", 33, 44]
		
		删除在替换新元素:
		var a = ["aa", 33, 44];
		a.splice(0,2,'hello','world');
		a --  ["hello", "world", 44];
	三个参数介绍:
		参数：1.从哪删(索引), 2.删几个  3.删除位置替换的新元素(可多个元素)
```



##### 自定义对象 -- python字典

```
索引取值
	var a = {'name':'alex','age':48};
	键可以不加引号:var a = {name:'alex',age:48};
	a['age']; -- 48
	a.age; -- 48
```



##### 类型查询

### ![img](https://images2015.cnblogs.com/blog/315302/201702/315302-20170205172450401-644571910.png)



### 运算符

##### 算数运算符

```
+ - * / % ++ --  i++,是i自加1，i--是i自减1   i++的这个加1操作优先级低，先执行逻辑，然后再自加1，而++i，这个加1操作优先级高，先自加1，然后再执行代码后面的逻辑

示例:
	var a = 100;
	a++;或者++a; -- 101 a自增1

	a++和++a的区别,示例:
	var a = 102;
	a++ == 102; -- true
	a -- 103;
	++a == 103; -- false
	a -- 104;

```

##### 比较运算符

```
> >= < <= != == === !==

==(弱等于)和===(强等于)两者的区别:
	示例:
		var a = 11;
		var b = '11';
		a == b -- true
         a === b; -- false

```

##### **逻辑运算符**

```
&& || !  #and，or，非（取反）!null返回true
示例:
	var a = true;
    var b = true;
    var c = false;
    a && b; -- true
    a && c; -- false
    a || c; -- true
    !c; -- true
```

##### 赋值运算符

```
= += -= *= /= 
示例: n += 1其实就是n = n + 1
```

### 流程控制

#### if判断

```
简单if-else判断
	var a = 4;
	if (a > 5){
        console.log('a大于5');

    }
    else{
        console.log('小于5');
    };

多条件判断
var a = 10;
if (a > 5){
  console.log("a > 5");
}else if(a < 5) {
  console.log("a < 5");
}else {
  console.log("a = 5");
}
```

#### switch 切换

```


示例:
	var a = 1;
	switch (a++){ //这里day这个参数必须是一个值或者是一个能够得到一个值的算式才行，这个值和后面写的case后面的值逐个比较，满足其中一个就执行case对应的下面的语句，然后break，如果没有加break，还会继续往下判断
        case 1:
            console.log('等于1');
            break;
        case 3:
            console.log('等于3');
            break;
        default:  case都不成立,执行default
            console.log('啥也不是!')	

    }
	

```

#### for循环

```
for (var i=0;i<10;i++) {  //就这么个写法，声明一个变量，变量小于10，变量每次循环自增1，for(;;){console.log(i)}；这种写法就是个死循环，会一直循环，直到你的浏览器崩了，就不工作了，回头可以拿别人的电脑试试~~
  console.log(i);
}
循环数组：
var l2 = ['aa','bb','dd','cc']
方式1
for (var i in l2){
   console.log(i,l2[i]);
}
方式2
for (var i=0;i<l2.length;i++){
　　console.log(i,l2[i])
}

循环自定义对象：
var d = {aa:'xxx',bb:'ss',name:'小明'};
for (var i in d){
    console.log(i,d[i],d.i)  #注意循环自定义对象的时候，打印键对应的值，只能是对象[键]来取值，不能使用对象.键来取值。
}


```

while循环

```
var i = 0;
var a = 10;
while (i < a){
	console.log(i);
	if (i>5){
		continue;
		break;
	}
	i++;
};
```

#### 三元运算

```
var c = a>b ? a:b;  
```

### 函数

#### 定义函数

```
普通函数
function f1(){
	console.log('111');
}
f1();  执行函数

带参数的函数
function f1(a,b){
	console.log('111');
}
f1(1,2);

带返回值的函数
function f1(a,b){
	return a+b;
}
f1(1,2); -- 3

返回值不能有多个
function f1(a,b){
	return a,b;
}
f1(1,2); -- 2
function f1(a,b){
	return [a,b];  想要多个返回值,需要换一种数据类型
}
f1(1,2); -- [1, 2]

匿名函数:
	var f1 = function(){
        console.log('111');
    }
    f1();

自执行函数
    (function(a,b){
        console.log(a+b);
    })(1,2);
```



#### 函数的全局变量和局部变量

```
局部变量：

	在JavaScript函数内部声明的变量（使用 var）是局部变量，所以只能在函数内部访问它（该变量的作用域是函数内部）。只要函数运行完毕，本地变量就会被删除。

全局变量：

	在函数外声明的变量是*全局*变量，网页上的所有脚本和函数都能访问它。

变量生存周期：

    JavaScript变量的生命期从它们被声明的时间开始。

    局部变量会在函数运行以后被删除。

    全局变量会在页面关闭后被删除。
```



#### 作用域

```
首先在函数内部查找变量，找不到则到外层函数查找，逐步找到最外层。

var city = "BeiJing";
function f() {
  var city = "ShangHai";
  function inner(){
    var city = "ShenZhen";
    console.log(city);
  }
  inner();
}
f();  


var city = "BeiJing";
function Bar() {
  console.log(city);
}
function f() {
  var city = "ShangHai";
  return Bar;
}
var ret = f();
ret();

闭包:
	var city = "BeiJing";
    function f(){
        var city = "ShangHai";
        function inner(){
            console.log(city);
        }
        return inner;
    }
    var ret = f();
    ret();

```

#### 面向对象

```
function Person(name){
	this.name = name;
};

var p = new Person('taibai');  

console.log(p.name);

Person.prototype.sum = function(a,b){  //封装方法
	return a+b;
};

p.sum(1,2);
3
```



### date对象

```
//方法1：不指定参数
var d1 = new Date(); //获取当前时间
console.log(d1.toLocaleString());  //当前2时间日期的字符串表示
//方法2：参数为日期字符串
var d2 = new Date("2004/3/20 11:12");
console.log(d2.toLocaleString())

常用方法
var d = new Date(); 
使用 d.getDate()
//getDate()                 获取日
//getDay ()                 获取星期 ，数字表示（0-6），周日数字是0
//getMonth ()               获取月（0-11,0表示1月,依次类推）
//getFullYear ()            获取完整年份
//getHours ()               获取小时
//getMinutes ()             获取分钟
//getSeconds ()             获取秒
//getMilliseconds ()        获取毫秒
//getTime ()                返回累计毫秒数(从1970/1/1午夜),时间戳

```



# 今日内容

### JSON对象

```
var a = {'name':'太白','age':89};
序列化:var b = JSON.stringify(a);
反序列化:var c = JSON.parse(b);
```



### RegExp对象

```

var reg1 = new RegExp("^[a-zA-Z][a-zA-Z0-9_]{5,11}$"); 
// 简写方式
var reg2 = /^[a-zA-Z][a-zA-Z0-9_]{5,11}$/; 

坑:
	reg2.test(); 什么也不填写,会默认成reg2.test('undefined');
	如果'undefined'满足你的正则要求,就返回true


字符串使用正则是的一些方法
var s2 = "hello world";
s2.match(/o/);  匹配元素
s2.match(/o/g);  加上g是全局匹配
s2.search(/o/);  找符合正则规则的字符串的索引位置
s2.split(/o/);  用符合正则的字符串进行分割

var s3 = 'Alex is A xiaosb';
s3.replace(/a/gi,'DSB');  替换,g全局替换,i不区分大小写


正则加g之后,进行test测试需要注意的问题

var reg3 = /a/g;
var s1 = 'alex is a xiaosb';
reg3.lastIndex -- 0
reg3.test(s1); -- true
reg3.lastIndex -- 1
reg3.test(s1); -- true
reg3.lastIndex -- 9
reg3.test(s1); -- true
reg3.lastIndex -- 13

reg3.test(s1); -- false

置零:
	reg3.lastIndex = 0
```



### Math对象

```

Math.abs(x)      返回数的绝对值。
exp(x)      返回 e 的指数。
floor(x)    小数部分进行直接舍去。
log(x)      返回数的自然对数（底为e）。
max(x,y)    返回 x 和 y 中的最高值。
min(x,y)    返回 x 和 y 中的最低值。
pow(x,y)    返回 x 的 y 次幂。
random()    返回 0 ~ 1 之间的随机数。
round(x)    把数四舍五入为最接近的整数。
sin(x)      返回数的正弦。
sqrt(x)     返回数的平方根。
tan(x)      返回角的正切。
```



# BOM对象

```
location对象
    location.href  获取URL
    location.href="URL" // 跳转到指定页面
    location.reload() 重新加载页面,就是刷新一下页面


定时器
    1. setTimeOut()  一段时间之后执行某个内容,执行一次
        示例 
            var a = setTimeout(function f1(){confirm("are you ok?");},3000);
            var a = setTimeout("confirm('xxxx')",3000);  单位毫秒
        清除计时器
            clearTimeout(a);  
    2.setInterval()  每隔一段时间执行一次,重复执行 
        var b = setInterval('confirm("xxxx")',3000);单位毫秒
        清除计时器
            clearInterval(b);

```



# DOM对象

## 查找标签

### 直接查找

```
document.getElementById           根据ID获取一个标签
document.getElementsByClassName   根据class属性获取（可以获取多个元素，所以返回的是一个数组）
document.getElementsByTagName     根据标签名获取标签合集

示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

        <div class="c1" id="d1">
            are you ok?

        </div>

        <div class="c1 c2">
            div2
        </div>
    </body>
    </html>

操作:
	var divEle = document.getElementById('d1');
	var divEle = document.getElementsByClassName('c1');
	var divEle = document.getElementsByTagName('div');
```



### 间接查找

```
parentElement            父节点标签元素
children                 所有子标签
firstElementChild        第一个子标签元素
lastElementChild         最后一个子标签元素
nextElementSibling       下一个兄弟标签元素
previousElementSibling   上一个兄弟标签元素
示例:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

        <div class="c1" id="d1">
            are you ok?

            <span id="s1">span1</span>
            <span id="s2">span2</span>
            <span id="s3">span3</span>
        </div>

        <div class="c1 c2">
            div2
        </div>
    </body>

操作:
	var divEle = document.getElementById('d1');
	找父级:divEle.parentElement;
	找儿子们:divEle.children;
	找第一个儿子:divEle.firstElementChild;
	找最后一个儿子:divEle.lastElementChild;
	找下一个兄弟:divEle.nextElementSibling;
```



### 标签操作

```
创建标签:重点
	var aEle = document.createElement('a');

添加标签
    追加一个子节点（作为最后的子节点）
    somenode.appendChild(newnode)；
	示例:
		var divEle = document.getElementById('d1')
		divEle.appendChild(aEle)
	
	
    把增加的节点放到某个节点的前边。
    somenode.insertBefore(newnode,某个节点);
	示例:
		var divEle = document.getElementById('d1'); 找到父级标签div
		var a = document.createElement('a');  创建a标签
		a.innerText = 'baidu';  添加文本内容
		var span2 = document.getElementById('s2'); 找到div的子标签span2
		divEle.insertBefore(a,span2); 将a添加到span2的前面
		
html文件代码:
	<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>

    <div class="c1" id="d1">
        are you ok?

        <span id="s1">span1</span>
        <span id="s2">span2</span>
        <span id="s3">span3</span>
    </div>

    <div class="c1 c2">
        div2
    </div>

    </body>
    </html>
		
```



删除节点

```
获得要删除的元素，通过父元素调用该方法删除。
somenode.removeChild(要删除的节点)
示例: 删除span2标签
    var divEle = document.getElementById('d1');
    var span2 = document.getElementById('s2');
    divEle.removeChild(span2);
```

替换节点：

```

somenode.replaceChild(newnode, 某个节点);
somenode是父级标签，然后找到这个父标签里面的要被替换的子标签，然后用新的标签将该子标签替换掉
```



### 文本节点操作

```
var divEle = document.getElementById("d1")
divEle.innerText  #输入这个指令，一执行就能获取该标签和内部所有标签的文本内容
divEle.innerHTML  #获取的是该标签内的所有内容，包括文本和标签
取值示例:
	div2.innerText;  不识别标签
    	"are you ok? span1 span2 span3"
    div2.innerHTML;  识别标签
        "
            are you ok?

            <span id="s1">span1</span>
            <span id="s2">span2</span>
            <span id="s3">span3</span>
        "
设置值:
	var div1 = document.getElementById('d1');
    div1.innerText = 'xxx';
    div1.innerText = '<a href="">百度</a>';
	div1.innerHTML = '<a href="">百度</a>';
```



### 属性操作

```
var divEle = document.getElementById("d1");
divEle.setAttribute("age","18")  #比较规范的写法
divEle.getAttribute("age")
divEle.removeAttribute("age")

// 自带的属性还可以直接.属性名来获取和设置，如果是你自定义的属性，是不能通过.来获取属性值的
imgEle.src
imgEle.src="..."

示例:
	<a href="http://www.baidu.com">百度</a>
	操作
		var a = document.getElementsByTagName('a');
		a[0].href;  获取值
		a[0].href = 'xxx'; 设置值
```



### 获取值操作

```
输入框 input
	获取值
		var inpEle = document.getElementById('username');
		inpEle.value;  
	设置值
		inpEle.value = 'alexDsb';

select选择框
	获取值
		var selEle = document.getElementById('select1');
		selEle.value;
     设置值
     	selEle.value = '1';
     	

```



### 类操作

```
className  获取所有样式类名(字符串)

首先获取标签对象
标签对象.classList; 标签对象所有的class类值

标签对象.classList.remove(cls)  删除指定类
classList.add(cls)  添加类
classList.contains(cls)  存在返回true，否则返回false
classList.toggle(cls)  存在就删除，否则添加，toggle的意思是切换，有了就给你删除，如果没有就给你加一个


示例:
	var divEle = document.getElementById('d1');
	divEle.classList.toggle('cc2');  
	var a = setInterval("divEle.classList.toggle('cc2');",30);

	判断有没有这个类值的方法
		var divEle = document.getElementById('d1');	
		divEle.classList.contains('cc1');
```



### css设置

```
1.对于没有中横线的CSS属性一般直接使用style.属性名即可。
2.对含有中横线的CSS属性，将中横线后面的第一个字母换成大写即可

设置值:
	divEle.style.backgroundColor = 'yellow';
获取值
	divEle.style.backgroundColor;
```



## 事件

```
简单示例:
	<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>

        <style>
            .cc1 {
                width: 100px;
                height: 100px;
                background-color: red;
            }
            .cc2{
                background-color: green;
            }

        </style>
    </head>
    <body>

    <div class="cc1 xx xx2" id="d1">

    </div>


    <script>
        var divEle = document.getElementById('d1');
        divEle.onclick = function () {
            divEle.style.backgroundColor = 'purple';
        }


    </script>
    </body>
    </html>
```



### 绑定事件的方式

```
方式1:
    <script>
        var divEle = document.getElementById('d1');  1.找到标签
        divEle.onclick = function () {       2.给标签绑定事件
            divEle.style.backgroundColor = 'purple';
        }
    </script>
    
    	下面的this表示当前点击的标签
        var divEle = document.getElementById('d1');
        divEle.onclick = function () {
            this.style.backgroundColor = 'purple';
        }
    
方式2
	标签属性写事件名称=某个函数();
	<div class="cc1 xx xx2" id="d1" onclick="f1();"></div>
	
    <script>
    	js里面定义这个函数
        function f1() {
            var divEle = document.getElementById('d1');
            divEle.style.backgroundColor = 'purple';
        }
    </script>
    
    
    获取当前操作标签示例,this标签当前点击的标签
    	<div class="cc1 xx xx2" id="d1" onclick="f1(this);"></div>
        function f1(ths) {
            ths.style.backgroundColor = 'purple';
        }
    
```









































































































































