
import functools

def wrapper(f):
	@functools.wraps(f)
	def inner(*args,**kwargs):
		return f(*args,**kwargs)
	return inner

"""
1. 执行wrapper
2. 重新赋值
index = wrapper(index)
"""
@wrapper
def index(a1,a2):
	"""
	这是一个index函数
	"""
	return a1+ a2


print(index.__name__)
print(index.__doc__)