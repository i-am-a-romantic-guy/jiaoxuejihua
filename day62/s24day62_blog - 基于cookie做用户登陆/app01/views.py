from django.shortcuts import render,redirect
from app01 import models

def login(request):
    """
    用户登录
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'login.html')

    # 获取用户提交的用户名和密码
    user = request.POST.get('user')
    pwd = request.POST.get('pwd')

    # 去数据库检查用户名密码是否正确
    # user_object = models.UserInfo.objects.filter(username=user,password=pwd).first()
    # user_object = models.UserInfo.objects.filter(username=user, password=pwd).exists()
    user_object = models.UserInfo.objects.filter(username=user, password=pwd).first()

    if user_object:
        # 用户登录成功
        result = redirect('/index/')
        result.set_cookie('xxxxxxxx',user)
        return result

    # 用户名或密码输入错误
    return render(request,'login.html',{'error':'用户名或密码错误'})


def index(request):
    """
    博客后台首页
    :param request:
    :return:
    """
    user = request.COOKIES.get('xxxxxxxx')
    if not user:
        return redirect('/login/')

    return render(request,'index.html',{'user':user})