# def func():
#     print(1)
#     def f1():
#         print(2)
#     return f1()
# func()

# 不管在什么位置,只要是函数名+() 就是在调用此函数
# 函数调用执行后,函数体中开辟的空间就自动销毁了

# def func():
#     a = 1
#     def foo():
#         b = 2
#         print(b)  # 2
#         print(a)  # 1
#         def f1():
#             print(b) # 2
#         return f1()
#     return foo()
# print(func())

# 函数互相引用

# def func():
#     a = 1
#     foo()
#     print(a)
#
# def foo():
#     b = 2
#     print(b)
# func()

# def a():
#     a1 = 1
#     c()
#     print(c)
# def b():
#     b1 = 2
#     print(b1)
# def c():
#     c1 = 3
#     print(a)
# def run():
#     a()
# run()


# def func():
#     a = 1
#     def b():
#         print(a)
#
# def foo():
#     b = 1
#     def z():
#         print(func)
#         print(b)
#     ret = z()
#     func()
#     return ret
# 
# def run():
#     foo()
# print(run())


# def func(a):
#     foo(a) #10
#
# def foo(e):
#     b(e) # return 10
#
# def b(c):
#     print(c)
#     return 10
#
# print(func(5))