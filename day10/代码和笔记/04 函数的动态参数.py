# def func(a,b=1):
#     print(a,b)
#
# func(1)

# def func(a,b,*args):  # *args 是万能(接受任意多个)的位置参数 *在函数定义的时候叫做聚合
#     print(a,b,args)
#
# func(1,2,3,4,5,6,7,8,9,0)

# def func(a,b,*args):  # *args 是万能(接受任意多个)的位置参数 *在函数定义的时候叫做聚合
#     print(a,b,*args)  # * 打散
#
# func(1,2,3,4,5,6,7,8,9,0)
# # 位置参数 > 动态位置参数 > 默认参数

# def func(*args,a=1,b=2):
#     print(a,b,args)  # 元组(接受的位置参数)
# func(12,15,16,19,10)

# def func(a,b,**kwargs):
#     print(a,b,kwargs)  # 字典(接受的关键字的参数)
# func(1,2,c=1,d=2)

# def func(a,b,*args,c=1,d=2):
#     print(a,b,*args,c,d)
# func(1,2,3,4)

# def func(a,b,*args,c=1,d=2,**kwargs):
#     print(a,b,*args,c,d,kwargs)   # *kwargs -- 获取到的字典的键
# func(1,2,3,4,c=8,d=10,e="alex",f="wusir")

# def func(*agrs,**kwargs):  # 万能传参
#     print(agrs,kwargs)
# func(1,2,3,4,5,6,7,8,a=1,b=2,c=3)

# 参数的优先级: 位置参数 > 动态位置参数 > 默认参数(关键字参数) > 动态关键字参数
# 1.万能传参: 动态位置参数,动态关键字参数 (最常用******)
# 2.位置参数,动态位置参数
# 3 动态位置参数,关键字参数,动态关键字参数
