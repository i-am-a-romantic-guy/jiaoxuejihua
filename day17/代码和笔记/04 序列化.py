# 什么是序列化 -- json
# lit = [1,22,3,3,45]  # [1,22,3,3,45]
# s_lst = str(lit)
# print(s_lst,type(s_lst))
# print(list(s_lst))
# print(eval(s_lst))  # 禁止使用

import json
# 两组4个方法:
#     1.dumps loads
            # lit = [1,22,3,3,45]
            # print(json.dumps(lit),type(json.dumps(lit)))
            # str_lst = json.dumps(lit)
            # lst = json.loads(str_lst)
            # print(lst,type(lst))

            # dumps 将对象转换(序列化)成字符串
            # loads 将字符串转换(反序列化)成对象

            # dic = {'username':'宝元'}
            # str_dic = json.dumps(dic)  # 序列化
            # str_dic = json.dumps(dic,ensure_ascii=False)  # ensure_ascii=False 关闭ascii码
            # print(str_dic,type(str_dic))
            # dic1 = json.loads(str_dic)  # 反序列化
            # print(dic1,dic1["username"])

#     2.dump load
            # import json
            # lit = [1,22,3,3,45]
            # 同时写多个内容 进行序列化
            # lst = [1,2,3,4,56,]
            # f = open("info","w",encoding="utf-8")
            # f.write(json.dumps(lst) + "\n")
            # f.write(json.dumps(lst) + "\n")
            # f.write(json.dumps(lst) + "\n")
            # f.write(json.dumps(lst) + "\n")
            # f.write(json.dumps(lst) + "\n")
            # f.close()

            # dump: 将对象转换(序列化)成字符串,写入文件
            # load: 将文件中字符串转换(反序列)成对象

            # 同时读多个内容进行反序列
            # f1 = open("info","r",encoding="utf-8")
            # for i in f1:
            #     l = json.loads(i)
            #     print(l)
            # f1.close()

# pickle 序列化 - nb(python所有对象进行转换)
# python自带的(只有python可以用)
# 两组4个方法:
#1. dumps loads
        # import pickle

        # lst = [12,3,4,5,768]
        # t_list = pickle.dumps(lst) # 转换成类似字节
        # print(t_list)
        # print(pickle.loads(t_list)[-1])

        # dic = {"user":"郭宝元"}
        # t_list = pickle.dumps(dic) # 转换成类似字节
        # print(t_list)
        # print(pickle.loads(t_list))

        # def func():
        #     print(111)

        # import json
        # fun = json.dumps(func)
        # print(fun)

        # fun = pickle.dumps(func)
        # print(fun)
        # pickle.loads(fun)()

#2. dump  load
# import pickle
# dic = {"usern":"baoyuian"}
# dic = {"usern":"宝元"}
# pickle.dump(dic,open("info","wb"))
# print(pickle.load(open("info","rb")))

# import pickle
# dic = {"user":"123"}
# pickle.dump(dic,open("info","ab"))

# import pickle
# dic = {"1":2}
# f = open("info","wb")
# s = "\n".encode("utf-8")
# f.write(pickle.dumps(dic)+ s)
# f.write(pickle.dumps(dic)+ s)
# f.write(pickle.dumps(dic)+ s)
# f.close()
#
# f1 = open("info","rb")
# for i in f1:
#     print(pickle.loads(i))

# 推荐使用json
#   json是各种语言通用的
#   pickle(python私有)
# asdfasd


