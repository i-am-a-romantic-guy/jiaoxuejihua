"""
1.序列化:
    json:
        dumps loads  对象转成字符串
        dump load    对象转成字符串写入文件
        
    pickle:
        dumps loads  对象转成类似字节
        dump  load    对象转成类似字节写入文件
2.os
    工作目录:
        os.getcwd()
        os.chdir()
    文件夹
        os.mkdir()
        os.rmdir()
        os.makedirs() 
        os.removedirs()
        os.listdir()
    文件
        os.rename
        os.remove
    路径
        os.path.abspath()
        os.path.join()
        os.path.dirname()
        os.path.basename()
        os.path.isabs()
        os.path.isfile()
        os.path.isdir()
        os.path.exists() 
        os.path.getsize() 
        
3.sys:
    sys.argv 当做脚本执行的时候能够携带参数
    sys.exit()
    sys.path()
    sys.platform()
    
4.hashlib:
    md5,sha1,sha256,sha512
    加密 - 校验
    
    import hashlib
    md5 = hashlib.md5()
    md5.update(b"alex")
    md5.hexdigest()
    
    加盐: 加固定盐,动态盐
    import hashlib
    md5 = hashlib.md5(b"wusir")
    md5.update(b"alex")
    md5.hexdigest()

5.collections
    1.命名元组
    2.默认字典参数
    3.双端队列
    4.计数器 ***   


""""