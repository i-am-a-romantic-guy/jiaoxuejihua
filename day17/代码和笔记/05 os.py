# os 模块 -- 程序员通过python向操作系统发送指令(与操作系统交互的接口)
# os模块四组:
#     1.工作目录
            # import os
            # print(os.getcwd()) # 当前工作路径  ***
            # os.chdir("F:\s24\day06") # 路径切换
            # print(os.curdir)  # 当前
            # print(os.pardir)  # 父级

#     2.文件夹
            # import os
            # os.mkdir("ttt") # 创建一个文件夹  ***
            # os.rmdir("ttt") # 删除一个文件夹  ***
            # os.makedirs("ttt/sss/ddd/ee")  # 递归创建文件夹     ***
            # os.removedirs("ttt/sss/ddd/ee")  # 递归删除文件夹   ***
            # print(os.listdir(r"F:\s24\day17"))  ***

#     3.文件
            # import os
            # os.rename()  # 修改名字   ***
            # os.remove("info") # 删除文件  ***

#     4.路径
            import os
            # print(os.path.abspath(r"01 今日内容.py"))  # 通过相对路径获取绝对路径  ***
            # print(os.path.split(os.path.abspath(r"01 今日内容.py")))  #将路径以最后一个\切割(路径,文件名)
            # print(os.path.dirname(r"F:\s24\day17\01 今日内容.py"))  # 获取路径 ***
            # print(os.path.basename(r"F:\s24\day17\01 今日内容.py")) # 获取文件名 **
            # print(os.path.exists(r"F:\s24\day17\01 今日内容.py"))  # 判断这个路径是否存在  ***
            # print(os.path.isdir(r"F:\s24\day17"))     # 判断是不是路径  ***
            # print(os.path.isfile(r"01 今日内容.py"))  # 判断是不是文件  ***
            # print(os.path.isabs(r"F:\s24\day17\01 今日内容.py"))     # 判断是不是绝对路径
            # print(os.path.join("D:\\\\","ttt","bbb"))                # 路径拼接 *****
            # import time
            # print(time.time())
            # print(os.path.getatime(r"F:\s24\day17\04 序列化.py"))      # 最后的修改时间
            # print(os.path.getctime(r"F:\s24\day17\04 序列化.py"))      # 最后的访问时间
            # print(os.path.getmtime(r"F:\s24\day17\04 序列化.py"))      # 最后的访问时间
            # print(os.path.getsize(r"F:\s24\day09"))                    # 获取当前文件的大小   ***
