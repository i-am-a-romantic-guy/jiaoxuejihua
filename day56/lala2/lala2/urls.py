"""lala2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from app01 import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^home/', views.home),  #http://127.0.0.1:8000/   index/
    url(r'^login/', views.login), #路径分发   不区别请求方法
    url(r'^login2/', views.login2),


    url(r'^login2/', views.login2),
    # ('/home',home)

    # 无名分组参数
    url(r'^articles/(\d+)/(\d+)/', views.articles), #articles/2019/9/

    # 有名分组参数
    url(r'^articles/(?P<xx>\d+)/(?P<oo>\d+)/', views.articles), #articles/2019/9/
    #xx=2019  oo=9  关键字传参

]


'''
    for i in urlpatterns:
        
        i.reg -- ^home/'
        
        request.path -- /home/
    
        ret = re.search(i.reg,request.path)
        if ret:
        
            i.home()
            break
            
    ('^/home',home)
    paht = conn.recv(1024).decode('utf-8').splite(' ')[1] == '/home'
    for url in urlpatterns:
        if ret.search(url[0],paht):
            url[1]()
            
'''



