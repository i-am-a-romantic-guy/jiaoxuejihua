

import pymysql


conn = pymysql.connect(
    host='127.0.0.1',
    port=3306,
    user='root',
    password='666',
    database='day43',
    charset='utf8',

)

cursor = conn.cursor(pymysql.cursors.DictCursor)
#默认游标取出的数据是 ((),)
# DictCursor 对应的数据结构 [{},],如果用的是fetcheone,那么结果是{}.
# sql = "select * from dep;"
sql = "select * from dep;"

ret = cursor.execute(sql) #ret 受影响的行数

print(ret)
# print(cursor.fetchmany())  #不写参数,默认是一条
print(cursor.fetchone())
# print(cursor.fetchall())

print('----------')
# cursor.scroll(2,'absolute')  #absolute 绝对移动,相对于数据最开始的位置进行光标的移动
cursor.scroll(2,'relative')  #relative 相对移动,按照光标当前位置来进行光标的移动

print(cursor.fetchone())












