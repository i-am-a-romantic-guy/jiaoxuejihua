# class Person:
#     name = 'aaa'
#
# p1 = Person()
# p2 = Person()
#
# p1.name = 'bbb'
#
# print(p1.name)
# print(p2.name)
# print(Person.name)

# 对象如果改变了类的静态属性, 具体他进行了什么操作?
    # 将类中的静态属性变成可变的数据类型.
    # 对象调用类的方法,方法中对类的属性进行改变.

# class A:
#     name = 'alex'
#     l1 = []
#     def func(self):
#         A.name = '太白'
#
# obj = A()
# obj.func()



# class A:
#
#     def __init__(self):
#         self.__func()  # self._A__func()
#
#     def __func(self):  # _A__func()
#         print('in A __func')
#
#
# class B(A):
#
#     def __func(self):  # _B__func()
#         print('in B __func')
#
#
# obj = B()


class Init(object):

    def __init__(self, value):  # 4
        self.val = value


class Add2(Init):

    def __init__(self, val):  # 3
        super(Add2, self).__init__(val)
        self.val += 2  # self.val = 7


class Mul5(Init):

    def __init__(self, val):  # 2
        super(Mul5, self).__init__(val)
        self.val *= 5  # self.val = 35


class Pro(Mul5, Add2):
    pass


class Iner(Pro):
    # csup = super(Pro)

    def __init__(self, val):  # 1
        # self.csup.__init__(val)
        super(Pro,self).__init__(val)
        self.val += 1

# [Iner, Pro, Mul5, Add2, Init]
p = Iner(5)
print(p.val)