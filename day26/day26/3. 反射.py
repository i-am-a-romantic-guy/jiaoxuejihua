# 程序对自己内部代码的一种自省方式.
# 反射是什么?  通过字符串去操作对象的方式.


# 实例对象.
# 类.
# 本模块.
# 其他模块.

# hasattr,getattr, setattr, delattr

# 实例对象:

# class A:
#
#     country = '中国'
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def func(self):
#         print('in A func')

# obj = A('赵海狗',47)

# hasattr
# print(hasattr(obj,'name'))
# print(hasattr(obj,'country'))
# print(hasattr(obj,'func'))

# print(getattr(obj,'name'))
# print(getattr(obj,'func'))
# f = getattr(obj,'func')
# f()
# print(getattr(obj,'sex',None))
# if hasattr(obj,'name'):
#     getattr(obj,'name')

# setattr,delattr 用的很少
# obj.sex = '公'
# print(obj.sex)
# setattr(obj,'sex','公')
# print(obj.__dict__)
# delattr(obj,'name')
# print(obj.__dict__)


# 从类的角度:

# class A:
#
#     country = '中国'
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def func(self):
#         print(self)
#         print('in A func')
#
# # if hasattr(A,'country'):
# #     print(getattr(A,'country'))
#
#
# if hasattr(A,'func'):
#     obj = A('赵海狗', 26)
#     getattr(obj,'func')()
#     getattr(A,'func')(obj)


# 从其他模块:
# import tbjx
# print(getattr(tbjx,'name'))
# getattr(tbjx,'func')()
import tbjx
# 1. 找到tbjx对象 的C类,实例化一个对象.
# print(getattr(tbjx,'C'))
# obj = getattr(tbjx,'C')('123')
# 2. 找到tbjx对象 的C类,通过对C类这个对象使用反射取到area.
# print(getattr(tbjx.C,'area'))
# 3. 找到tbjx对象 的C类,实例化一个对象,对对象进行反射取值.
# obj = getattr(tbjx,'C')('赵海狗')
# print(obj.name)
# print(getattr(obj,'name'))
# 从当前模块研究反射:
# a = 666
#
# def func1():
#     print('in 本模块这个对象')


# def func1():
#     print('in func1')
#
# def func2():
#     print('in func2')
#
# def func3():
#     print('in func3')
#
# def func4():
#     print('in func4')

# # func1()
# # func2()
# # func3()
# # l1 = [func1,func2,func3,func4,]
# import sys
# # # print(sys.modules[__name__])
# # print(getattr(sys.modules[__name__],'a'))
# # getattr(sys.modules[__name__],'func1')()
# # getattr(sys.modules[__name__],'func2')()
# # getattr(sys.modules[__name__],'func3')()
#
# func_lst = [f'func{i}' for i in range(1,5)]
# # print(func_lst)
# for func in func_lst:
#     getattr(sys.modules[__name__],func)()


# class User:
#     def login(self):
#         print('欢迎来到登录页面')
#
#     def register(self):
#         print('欢迎来到注册页面')
#
#     def save(self):
#         print('欢迎来到存储页面')
#
# choose_dic = {
#     1: User.login,
#     2: User.register,
#     3: User.save,
# }
#
# while 1:
#     choose = input('请输入序号: \n1: 登录\n2: 注册\n3: 存储').strip()
#     obj = User()
#     choose_dic[int(choose)](obj)


# class User:
#
#     user_list = [('login','登录'),('register','注册'),('save', '存储')]
#
#     def login(self):
#         print('欢迎来到登录页面')
#
#     def register(self):
#         print('欢迎来到注册页面')
#
#     def save(self):
#         print('欢迎来到存储页面')
#
#
# while 1:
#     choose = input('请输入序号: \n1: 登录\n2: 注册\n3: 存储\n').strip()  # 1
#     obj = User()
#     getattr(obj, obj.user_list[int(choose)-1][0])()  # getattr(obj,'login')


