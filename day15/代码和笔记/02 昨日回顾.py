# 1.装饰器
# 1.1 开放封闭原则: 在不修改源代码和调用方式的基础上,额外增加任意功能!

# def func(f):
#     def foo(*args,**kwargs):
#         f(*args,**kwargs)
#     return foo
#
# @func
# def f1():
#     print(123)
# f1()

# def func(f):
#     def foo(*args,**kwargs):
#         print("装饰前")
#         ret = f(*args,**kwargs)
#         print("装饰后")
#         return ret
#     return foo
#
# @func
# def f1(*args,**kwargs):
#     print(args,kwargs)
#     return "被调用了"
#
# print(f1(1,2,a=1,b=2))

# @func  f1 = func(f1)
