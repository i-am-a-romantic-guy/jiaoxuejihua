"""
1.请将列表中的每个元素通过 "_" 链接起来。
users = ['大黑哥','龚明阳',666,'渣渣辉']
"""
# users = ['大黑哥','龚明阳',666,'渣渣辉']
# s = ""
# for i in users:
#     s = s + str(i) + "_"
# print(s[:-1])

"""
8.有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2....}
写代码
"""
# s = "k: 1|k1:2|k2:3 |k3 :4|k4:546|k5:2 3"
# dic = {}
# for i in s.replace(" ","").split("|"):
#     # ['k:1', 'k1:2', 'k2:3', 'k3:4']
#     k,v = i.split(":")
#     dic[k] = int(v)
# print(dic,type(dic))


"""
9.有如下值 li= [11,22,33,44,55,77,88,99,90] ,
将所有大于 66 的值保存至字典的第一个key对应的列表中，
将小于 66 的值保存至第二个key对应的列表中。
"""
# dic = {"key1":[],"key2":[]}
# li= [11,22,33,44,55,77,88,99,90]
# for i in li:
#     if i > 66:
#         dic["key1"].append(i)
#     else:
#         dic["key2"].append(i)
# print(dic)

"""
10.输出商品列表，用户输入序号，显示用户选中的商品

商品列表：
goods = [
{"name": "电脑", "price": 1999},
{"name": "鼠标", "price": 10},
{"name": "游艇", "price": 20},
{"name": "美女", "price": 998}
]
要求:
1：页面显示 序号 + 商品名称 + 商品价格，如：
1 电脑 1999
2 鼠标 10
3 游艇 20
4 美女 998
2：用户输入选择的商品序号，然后打印商品名称及商品价格
3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
4：用户输入Q或者q，退出程序。
"""

goods = [
{"name": "电脑", "price": 1999},
{"name": "鼠标", "price": 10},
{"name": "游艇", "price": 20},
{"name": "美女", "price": 998},
{"name": "常鑫", "price": 0.100}
]

# count = 1
# for i in goods:
#     print(count,i["name"],i["price"])
#     count += 1


# while True:
#     for i in range(len(goods)):
#         print(i+1,goods[i]["name"],goods[i]["price"])
#     print("*" * 50)
#
#     num = input("请输入序号(Q/退出):")  # 3
#     if num.isdecimal() and len(goods) >= int(num) > 0:
#         index = int(num) - 1
#         print(goods[index]["name"],goods[index]["price"])
#     elif num.upper() == "Q":
#         print("欢迎下次光临!")
#         break
#
#     else:
#         print("输入有误，并重新输入")

# 11.看代码写结果(一定要先看代码在运行)

# v = {}
# for index in range(10):
#     v['users'] = index
# print(v)