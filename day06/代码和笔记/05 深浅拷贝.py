# 深浅拷贝  复制

# 面试必问 ,赋值,浅拷贝,深拷贝

# lst = [1,2,3,[5,6,7]]
# lst1 = lst
# print(lst1)
# print(lst)
#
# lst[-1].append(8)
# print(lst1)
# print(lst)

#
# lst1 = lst.copy()  # 新开辟一个空间给lst1
#
# print(lst[-1])
# print(lst1[-1])
#
# print(id(lst1[-1]))
# print(id(lst[-1]))

# 浅拷贝的时候,只会开辟一个新的容器列表,其他元素使用的都是源列表中的元素

# lst = [1,2,3,[5,6,7]]
# lst1 = lst.copy()
# lst1[-1].append(8)
# print(lst)
# print(lst1)

# lst = [1,2,3,[5,6,7]]
# lst1 = lst.copy()
# lst[3] = 567
# print(lst1)
# print(lst)


# lst = [1,2,3,4,[5,6,7,[8,9]]]
# lst1 = lst.copy()  #  [1,2,3,4,[5,6,7,[8,9]],10]
# lst1.append(10)
# print(lst)
# print(lst1)

# lst = [1,2,3,4,[5,6,7,[8,9]]]
# lst1 = lst.copy()
# lst1[-1][-1] = "56"
# print(lst) # [1,2,3,4,[5,6,7,[8,9]]]
# print(lst1) # ["56",2,3,4,[5,6,7,[8,9]]]

# lst = [[1,2,],90,6,7,[5,6]]
# lst1 = lst.copy()
# lst1[-1] = 8
# print(lst)
# print(lst1)

# dic = {"alex":[1,2,3,[5,6]]}
# dic1 = dic.copy()
# dic["alex"][0] = "56"
# print(dic)
# print(dic1)

# 深拷贝
# import copy  #导入
# lst = [1,2,3,[5,6,7]]
# lst1 = copy.deepcopy(lst)  # 深拷贝
# lst[-1].append(8)
# print(lst)
# print(lst1)


# lst = [1,2,3,[5,6,7,[8,10,9]]]
# import copy
# lst1 = copy.deepcopy(lst)
# print(id(lst[-1][-1]))
# print(id(lst1[-1][-1]))

# 总结:

# 浅拷贝的时候只拷贝第一层元素
# 浅拷贝在修改第一层元素(不可变数据类型)的时候,拷贝出来的新列表不进行改变
# 浅拷贝在替换第一层元素(可变数据类型)的时候,拷贝出来的新列表不进行改变
# 浅拷贝在修改第一层元素中的元素(第二层)的时候,拷贝出来的新列表进行改变

# 深拷贝开辟一个容器空间(列表),不可变数据公用,可变数据数据类型(再次开辟一个新的空间)
# ,空间里的值是不可变的数据进行共用的,可变的数据类型再次开辟空间
