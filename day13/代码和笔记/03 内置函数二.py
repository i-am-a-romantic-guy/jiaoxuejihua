# print(abs()) # 绝对值 -- 返回的都是正数

# print([abs(i) for i in lst])

# enumerate -- 枚举 ("可迭代对象","序号的起始值")  # 默认的起始值是0
# [(0,1),(1,2),(2,3)]
# print([i for i in enumerate(lst,10)])

# lst = [11,22,33,-44,23,21]
# new_lst = []
# for i in enumerate(lst):
#     new_lst.append(i)
# print(new_lst)
# print([i for i in enumerate(lst,1000)])

# print(max([1,2,3,4,56,7,8])) # 求最大值
# print(min([1,2,3,4,-56,7,8])) # 求最小值
# print(sum([1,2,3,4,5],100))   # 求和

# python3:
# g = range(0,10)   # 可迭代对象
# g.__iter__()

# python2:
# range(0,10)       # 获取是一个列表
# xrange(0,10)      # 获取是一个可迭代对象


# from collections import Iterable,Iterator
# print(isinstance(g,Iterable))
# print(isinstance(g,Iterator))


# print(sep=" ",end="\n")
# print(1,2,3,sep="      ")  # sep多个元素的连接符
# print(1,end="\t")
# print(2,end=" ")
# print(3)

# print(12345,file=open("t1.txt","w",encoding="utf-8"))

# print(list("alex")) #['alex',]
# print(dict(key=1,a="alex"))

# print(dict(((1,2),(2,3),(3,4))))
# print(dict([i for i in enumerate(range(20),1)]))

# zip() # 拉链 -- 按照最少的进行合并

# lst1 = [1,2,3,4,5]
# lst2 = ['a',"b","c","d","f","e"]
# print(dict(list(zip(lst1,lst2))))  # 面试题
# print(dict(zip(lst1,lst2)))  # 面试题

# print(dir(list))  # 查看当前函数的方法
