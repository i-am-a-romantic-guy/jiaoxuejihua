

# 昨日内容回顾

## 基于双下划线的跨表查询（连表查询 join）

```
	select emp.name from emp inner join dep on emp.dep_id = dep.id where dep.name='技术';
	select emp.name from dep inner join emp on emp.dep_id = dep.id where dep.name='技术';
```



示例

```
# 一对一
# 查询一下王洋的电话号码

# ret = models.Author.objects.filter(name='王洋').values('au__telephone')

# ret = models.AuthorDetail.objects.filter(author__name='王洋').values('telephone')
# print(ret) #<QuerySet [{'au__telephone': '110'}]> #<QuerySet [{'telephone': '110'}]>
#

# 一对多
# 海狗的怂逼人生这本书是哪个出版社出版的
# ret = models.Book.objects.filter(title='海狗的怂逼人生').values('publishs__name')
# print(ret) #<QuerySet [{'publishs__name': '24期出版社'}]>
# ret = models.Publish.objects.filter(book__title='海狗的怂逼人生').values('name')
# print(ret) #<QuerySet [{'name': '24期出版社'}]>


#查询一下24期出版社出版了哪些书
# ret = models.Publish.objects.filter(name='24期出版社').values('book__title')
# print(ret) #<QuerySet [{'book__title': '华丽的产后护理'}, {'book__title': '海狗的怂逼人生'}]>

# ret = models.Book.objects.filter(publishs__name='24期出版社').values('title')
# print(ret) #<QuerySet [{'title': '华丽的产后护理'}, {'title': '海狗的怂逼人生'}]>

# 多对多
#海狗的怂逼人生 是哪些作者写的
# ret = models.Book.objects.filter(title='海狗的怂逼人生').values('authors__name')
# print(ret)

# ret = models.Author.objects.filter(book__title='海狗的怂逼人生').values('name')
# print(ret) #<QuerySet [{'name': '王洋'}, {'name': '海狗'}]>
# return render(request,'index.txt',{'x':123,'y':456})


# related_name
# 查询一下24期出版社出版了哪些书
ret = models.Publish.objects.filter(name='24期出版社').values('xxx__title') #xxx代替反向查询的小写表名
print(ret)
```



聚合查询

```
ret = models.Book.objects.all().aggregate(a=Avg('price'),m=Max('price'))
print(ret) 
#{'price__avg': 45.1, 'price__max': Decimal('200.00')} python字典格式,也就是说,聚合查询是orm语句的结束
```



分组查询

```
    # 每个出版社出版的书的平均价格
    # 用的是publish表的id字段进行分组
    # ret = models.Book.objects.values('publishs__id').annotate(a=Avg('price'))
    # 用的book表的publishs_id字段进行分组
    # ret = models.Book.objects.values('publishs_id').annotate(a=Avg('price'))
    # print(ret)
    # ret = models.Publish.objects.annotate(a=Avg('book__price')).values('a')
    # print(ret) #<QuerySet [{'a': None}, {'a': 71.166667}, {'a': 6.0}]>
```

F查询

```
from django.db.models import Avg, Sum, Max, Min, Count,F
查询一下评论数大于点赞数的书
# ret = models.Book.objects.filter(comment__gt=F('good'))
# print(ret)

将所有书的价格上调100块
# models.Book.objects.all().update(
#     price=F('price')+100
# )
```



Q查询

```
    from django.db.models import Avg, Sum, Max, Min, Count, F,Q
    ret = models.Book.objects.filter(Q(id=2)&Q(Q(price__gt=112)|~Q(comment__lte=200)))
    print(ret)
```





作业

```
表

class Author(models.Model):
    """
    作者表
    """
    name=models.CharField( max_length=32)
    age=models.IntegerField()
    # authorDetail=models.OneToOneField(to="AuthorDetail",to_field="nid",on_delete=models.CASCADE)
    au=models.OneToOneField("AuthorDetail",on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class AuthorDetail(models.Model):
    """
    作者详细信息表
    """
    birthday=models.DateField()
    telephone=models.CharField(max_length=11)
    addr=models.CharField(max_length=64)
    # class Meta:
        # db_table='authordetail' #指定表名
        # ordering = ['-id',]
    def __str__(self):
        return self.telephone + self.addr


class Publish(models.Model):
    """
    出版社表
    """
    name=models.CharField( max_length=32)
    city=models.CharField( max_length=32)
    def __str__(self):
        return self.name

class Book(models.Model):
    """
    书籍表
    """
    title = models.CharField( max_length=32)
    publishDate=models.DateField()
    price=models.DecimalField(max_digits=5,decimal_places=2)
    comment = models.FloatField(default=100) #评论数
    good = models.FloatField(default=100) #点赞数
    # publishs=models.ForeignKey(to="Publish",on_delete=models.CASCADE,related_name='xxx')
    publishs=models.ForeignKey(to="Publish",on_delete=models.CASCADE,)
    authors=models.ManyToManyField('Author',)

    def __str__(self):
        return self.title

```

题

```

#1 查询每个作者的姓名以及出版的书的最高价格
    
#2 查询作者id大于2作者的姓名以及出版的书的最高价格
   
#3 查询作者id大于2或者作者年龄大于等于20岁的女作者的姓名以及出版的书的最高价格

#4 查询每个作者出版的书的最高价格 的平均值
    
#5 每个作者出版的所有书的最高价格以及最高价格的那本书的名称

```



# 今日内容

## ajax

```
   特点:异步请求和局部刷新
   
   $('#sub').click(function () {

        var uname = $('#username').val();
        var pwd = $('#password').val();
        // $.ajax({
        	  url:'{% url "login" %}',#}
        //    type:'get',
        //    success:function (res) {
        //        console.log(res);
        //    }
        //})

        $.ajax({
            url:'{% url "login" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            data:{username:uname,password:pwd},
            headers:{
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                console.log(res);
                if (res === '1'){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text('用户名密码错误!');
                    
                }

            }

        })

    })







```



csrftoken

详述CSRF（Cross-site request forgery），中文名称：跨站请求伪造，也被称为：one click attack/session riding，缩写为：CSRF/XSRF。攻击者通过HTTP请求江数据传送到服务器，从而盗取回话的cookie。盗取回话cookie之后，攻击者不仅可以获取用户的信息，还可以修改该cookie关联的账户信息。

　　![img](https://img2018.cnblogs.com/blog/988061/201908/988061-20190804210821110-1699988322.png)

 

　　所以解决csrf攻击的最直接的办法就是生成一个随机的csrftoken值，保存在用户的页面上，每次请求都带着这个值过来完成校验。



### form表单

```
<form action="" method="post">
    {% csrf_token %}  // form表单里面加上这个标签,模板渲染之后就是一个input标签,type=hidden  name=csrfmiddlewaretoken  value='asdfasdfasdf'
    用户名: <input type="text" name="username">
    密码: <input type="password" name="password">
    <input type="submit">

</form>

```

### ajax过csrf认证

```
方式1
    $.ajax({
      url: "/cookie_ajax/",
      type: "POST",
      data: {
        "username": "chao",
        "password": 123456,
        "csrfmiddlewaretoken": $("[name = 'csrfmiddlewaretoken']").val()  // 使用jQuery取出csrfmiddlewaretoken的值，拼接到data中
      },
      success: function (data) {
        console.log(data);
      }
    })

方式2
	$.ajax({
        data: {csrfmiddlewaretoken: '{{ csrf_token }}' },
    });
方式3
	$.ajax({
 
		headers:{"X-CSRFToken":$.cookie('csrftoken')}, #其实在ajax里面还有一个参数是headers，自定制请求头，可以将csrf_token加在这里，我们发contenttype类型数据的时候，csrf_token就可以这样加
 
})

jquery操作cookie: https://www.cnblogs.com/clschao/articles/10480029.html

```



### form表单上传文件

```
<form action="" method="post" enctype="multipart/form-data">  别忘了enctype
    {% csrf_token %}
    用户名: <input type="text" name="username">
    密码: <input type="password" name="password">
    头像: <input type="file" name="file"> 

    <input type="submit">

</form>


views.py
def upload(request):

    if request.method == 'GET':
        print(settings.BASE_DIR) #/static/

        return render(request,'upload.html')

    else:
        print(request.POST)
        print(request.FILES)
        uname = request.POST.get('username')
        pwd = request.POST.get('password')

        file_obj = request.FILES.get('file')  #文件对象

        print(file_obj.name) #开班典礼.pptx,文件名称
		
        with open(file_obj.name,'wb') as f:
            # for i in file_obj:
            #     f.write(i)
            for chunk in file_obj.chunks():
                f.write(chunk)
        return HttpResponse('ok')


```



### ajax上传文件

```
  $('#sub').click(function () {
  
        var formdata = new FormData();
	    
        var uname = $('#username').val();
        var pwd = $('#password').val();

        var file_obj = $('[type=file]')[0].files[0]; // js获取文件对象

        formdata.append('username',uname);
        formdata.append('password',pwd);
        formdata.append('file',file_obj);

        $.ajax({
            url:'{% url "upload" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            //data:{username:uname,password:pwd},
            data:formdata,
            
            processData:false,  // 必须写
            contentType:false,  // 必须写

            headers:{
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                console.log(res);
                if (res === '1'){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text('用户名密码错误!');
                }

            }

        })

    })
```



jsonresponse

```
from django.http import JsonResponse


        username = request.POST.get('username')
        pwd = request.POST.get('password')
        ret_data = {'status':None,'msg':None}
        print('>>>>>',request.POST)
        #<QueryDict: {'{"username":"123","password":"123"}': ['']}>
        if username == 'chao' and pwd == '123':
            ret_data['status'] = 1000  # 状态码
            ret_data['msg'] = '登录成功'


        else:
            ret_data['status'] = 1001  # 状态码
            ret_data['msg'] = '登录失败'

        # ret_data_json = json.dumps(ret_data,ensure_ascii=False)
        # return HttpResponse(ret_data_json,content_type='application/json')
        
        
        return JsonResponse(ret_data)

```

```
 $.ajax({
            url:'{% url "jsontest" %}',
            type:'post',
            // data:{username:uname,password:pwd,csrfmiddlewaretoken:csrf},
            //data:JSON.stringify({username:uname,password:pwd}),

            data:{username:uname,password:pwd},
            headers:{
                // contentType:'application/json',
                "X-CSRFToken":$.cookie('csrftoken'),
            },
            success:function (res) {
                {#console.log(res,typeof res); // statusmsg {"status": 1001, "msg": "登录失败"}#}
                {#var res = JSON.parse(res);  //-- json.loads()#}
                console.log(res,typeof res);  //直接就是反序列化之后的了
                //JSON.stringify()  -- json.dumps
                if (res.status === 1000){
                    // $('.error').text('登录成功');
                    location.href = '/home/'; // http://127.0.0.1:8000/home/

                }else{
                    $('.error').text(res.msg);
                }

            }

        })

```









































