from django.shortcuts import render,HttpResponse
from app01 import models
# Create your views here.

def index(request):
    #基于对象的跨表查询
    # 查询
    # 一对一
    # 关系属性写在表1,关联到表2,那么通过表1的数据去找表2的数据,叫做正向查询,返过来就是反向查询
    # 查询一下王洋的电话号码

    # 正向查询  对象.属性
    # obj = models.Author.objects.filter(name='王洋').first()
    # ph = obj.au.telephone
    # print(ph)

    # 查一下电话号码为120的作者姓名
    # 反向查询  对象.小写的表名
    # obj = models.AuthorDetail.objects.filter(telephone=120).first()
    # ret = obj.author.name  #陈硕
    # print(ret)

    # 一对多
    # 查询一下 海狗的怂逼人生这本书是哪个出版社出版的  正向查询
    # obj = models.Book.objects.filter(title='海狗的怂逼人生').first()
    # ret = obj.publishs.name
    # print(ret)  #24期出版社
    #  查询一下 24期出版社出版过哪些书
    # obj = models.Publish.objects.filter(name='24期出版社').first()
    #
    # ret = obj.book_set.all() #<QuerySet [<Book: 母猪的产后护理>, <Book: 海狗的怂逼人生>]>
    # for i in ret:
    #     print(i.title)

    # 多对多
    # 海狗的怂逼人生 是哪些作者写的 -- 正向查询
    # obj = models.Book.objects.filter(title='海狗的怂逼人生').first()
    # ret = obj.authors.all()
    #
    # print(ret)  #<QuerySet [<Author: 王洋>, <Author: 海狗>]>
    # for i in ret:
    #     print(i.name)

    # 查询一下海狗写了哪些书 -- 反向查询
    # obj = models.Author.objects.filter(name='海狗').first()
    # ret = obj.book_set.all()
    # print(ret)
    # for i in ret:
    #     print(i.publishs.name)
    #     print(i.title)



    return HttpResponse('ok')







