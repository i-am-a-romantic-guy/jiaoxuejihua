
from wsgiref.simple_server import make_server

from urls import urlpatterns

def application(environ,start_response):
    # environ  所有请求相关信息
    # start_response --封装响应数据格式

    print('当前请求路径',environ['PATH_INFO'])
    path = environ['PATH_INFO']
    start_response('200 ok',[]) # conn.send(b'HTTP/1.1 200 ok\r\n\r\n')
    for url in urlpatterns:
        if path == url[0]:
            ret = url[1]()

    return [ret]


if __name__ == '__main__':
    h = make_server('127.0.0.1', 8080, application)
    h.serve_forever()






