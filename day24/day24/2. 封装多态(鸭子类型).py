# class Student:
#
#     def __init__(self,name,sex):
#         self.name = name
#         self.sex = sex
#
# liye = Student('李业','laddy_boy')
# print(liye.sex)


# 多态:
# python 一个变量可以指向多种数据.
# a = [1, 2, 3]
# a = 'sdfasdf'
'''
java:
int a = 123


def func(int x):
    print(x)


def func(x):
    print(x)
    
'''




# class A:
#
#     def login(self):
#         print('登录')
#
#     def register(self):
#         print('注册')
#
#     def func1(self):
#         pass
#
#     def func2(self):
#         pass
#
#
# class B:
#
#     def login(self):
#         print('登录')
#
#     def register(self):
#         print('注册')
#
#     def func3(self):
#         pass

# A B互为鸭子.
# 赵嘎:  1. 好记.
# 虽然A,B两个类没有关系,但是我统一两个类中相似方法的方法名,在某种意义上统一的标准.

# index ,index, index,

# str
# list
# a = [1,2,3]
# a = list('123')
# print(a)

# print('字体变色，但无背景色')
# 固定的头尾: \033[         \033[0m

# 1;35;0m 具体调节的参数
# print('\033[1;32;0m字体变色，但无背景色 \033[0m')  # 有高亮 或者 print('\033[1;35m字体有色，但无背景色 \033[0m')
# print('\033[1;33;0m字体变色，但无背景色 \033[0m')  # 有高亮 或者 print('\033[1;35m字体有色，但无背景色 \033[0m')
# print('\033[1;45m 字体不变色，有背景色 \033[0m')  # 有高亮
# print('\033[1;35;46m 字体有色，且有背景色 \033[0m')  # 有高亮
print('\033[0;35;0m 字体有色，且有背景色 \033[0m')  # 无高亮
print('\033[1;35;0m 字体有色，且有背景色 \033[0m')  # 无高亮
print('\033[4;35;0m 字体有色，且有背景色 \033[0m')  # 无高亮
# print('\033[5;35;0m 字体有色，且有背景色 \033[0m')  # 无高亮














