# encoding: utf-8
# 1.
# 简答题：
# 1.
# 面向对象的三大特性是什么？
# 2.
# 什么是面向对象的新式类？什么是经典类？
# 3.
# 面向对象为什么要有继承？继承的好处是什么？
# 4.
# 面向对象中super的作用。
#
#
#
# 2.
# 代码题(通过具体代码完成下列要求)：
#
# ```
#
#
# class A:
#     def func(self):
#         print('in A')
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#     def func(self):
#         print('in C')
#
#
# ```
#
# ​        可以改动上上面代码，完成下列需求：对C类实例化一个对象产生一个c1，然后c1.func()
# class A:
#     def func(self):
#         print('in A')
#
#
# class B:
#     def func(self):
#         print('in B')
#
#
# class C(A, B):
#     def func(self):
#         # A.func(self)
#         B.func(self)
#         print('in C')
#
# c1 = C()
# c1.func()
# c1.func()

# ​        1.
# 让其执行C类中的func
#
# ​        2.
# 让其执行A类中的func
#
# ​        3.
# 让其执行B类中的func
#
# ​        4.
# 让其既执行C类中的func，又执行A类中的func
#
# ​        5.
# 让让其既执行C类中的func，又执行B类中的func









# 3.
# 下面代码执行结果是什么？为什么？
#
# ```
#
#
# class Parent:
#     def func(self):
#         print('in Parent func')
#
#     def __init__(self):
#         self.func()
#
#
# class Son(Parent):
#     def func(self):
#         print('in Son func')
#
#
# son1 = Son()
# ```
# class Parent:
#     def func(self):
#         print('in Parent func')
#
#     def __init__(self):  # self = son1
#         self.func()  # son1.func()
#
# class Son(Parent):
#     def func(self):
#         print('in Son func')
#
# son1 = Son()

# 4.
#
# ```
#
#
# class A:
#     name = []
#
#
# p1 = A()
# p2 = A()
# p1.name.append(1)
# # p1.name，p2.name，A.name 分别是什么？
# p1.age = 12
# # p1.age，p2.age，A.age 分别又是什么？为什么？
# ```

# 类的静态属性如果指向一个可变的数据类型,对象或者类名都可以给这个可变的数据类型:增查改删.
class A:
    name = []


p1 = A()
p2 = A()
p1.name.append(1)
# p1.name，p2.name，A.name 分别是什么？
# p1.age = 12
# A.age = 12
# p1.age，p2.age，A.age 分别又是什么？为什么？
# 5.
# 写出下列代码执行结果：
#
# ```
#
#
# class Base1:
#     def f1(self):
#         print(**'base1.f1' **)
#
#     def f2(self):
#         print(**'base1.f2' **)
#
#     def f3(self):
#         print(**'base1.f3' **)
#         self.f1()
#
#
# class Base2:
#     def f1(self):
#         print(**'base2.f1' **)
#
#
# class Foo(Base1, Base2):
#     def f0(self):
#         print(**'foo.f0' **)
#         self.f3()
#
#
# obj = Foo()
# obj.f0()
# class Base1:
#     def f1(self):
#         print('base1.f1' )  # 3
#
#     def f2(self):
#         print('base1.f2')
#
#     def f3(self):
#         print('base1.f3')  # 2
#         self.f1()
#
#
# class Base2:
#     def f1(self):
#         print('base2.f1')
#
#
# class Foo(Base1, Base2):
#     def f0(self):
#         print('foo.f0')  # 1
#         self.f3()


# obj = Foo()
# obj.f0()


# ```
#
# 5.
# 看代码写结果：
#
# ```
#
#
# class Parent:
#     x = 1
#
#
# class Child1(Parent):
#     pass
#
#
# class Child2(Parent):
#     pass
#
#
# print(Parent.x, Child1.x, Child2.x)
#
# Child2.x = 2
# print(Parent.x, Child1.x, Child2.x)
#
# Child1.x = 3
# print(Parent.x, Child1.x, Child2.x)
# ```

# class Parent:
#     x = 1
#
#
# class Child1(Parent):
#     pass
#
#
# class Child2(Parent):
#     pass
#
#
# print(Parent.x, Child1.x, Child2.x)  # 1 1 1
# Child2.x = 2
# print(Parent.x, Child1.x, Child2.x)  # 1 1 2
# Child1.x = 3
# print(Parent.x, Child1.x, Child2.x)  # 1 3 2



# 6.
# 有如下类：
#
# ```
#
#
# class A:
#     pass
# 
# 
# class B(A):
#     pass
# 
# 
# class C(A):
#     pass
# 
# 
# class D(A):
# 
#     def func(self):
#         print 'in D'
# 
# 
# class E(B, C):
#     pass
# 
# 
# class F(C, D):
#     pass
# 
# 
# class G(D):
#     pass
# 
# 
# class H(E, F):
#     pass
# 
# 
# class I(F, G):
# 
#     def func(self):
#         print 'in I'
# 
# 
# class K(H, I):
#     pass
# 
# obj = K()
# obj.func()

#
# ```
#
# 如果这是经典类，请写出他的继承顺序。
#
# 如果这是新式类，请写出他的继承顺序，并写出具体过程。
#
#
#
#
#
#
#
#
#
