1. 今日内容大纲

   1. 高大上版解决粘包方式（自定制包头）
   2. 基于UDP协议的socket通信
   3. socketserver

2. 昨日内容回顾

   1. 通信循环

   2. 链接通信循环

   3. 远程执行命令： subprocess.Popen()

      bytes: 网络传输, 文件存储时.

   4. 粘包现象
      1. 对方发来的数据大于自己recv的上线,下一次在recv会读取剩余的数据.
      2. 连续多次send数据量较小的数据,这些数据会粘在一起发送出去.

   5. 缓冲区: 输入缓冲区,输出缓冲区. 存储少量数据,避免网络不稳,造成你传输数据时的卡顿,保持相对平稳,稳定.

   6. 收发的本质:

      ​	不一定是一收一发.

   7. 如何解决粘包?

      low版: 制作一个固定的报头.

      获取总数据的长度. 7878

      利用struct模块将int 7878 ---> ret = 4个字节

      send(ret)

      send(总数据)

      客户端:

      head_bytes = recv(4)

      head = struct.unpack('i',head_bytes)[0]   7878

      利用while循环判断:

      ​	循环recv.

      

   

3. 今日内容

   1. recv工作原理

      ```
      源码解释：
      Receive up to buffersize bytes from the socket.
      接收来自socket缓冲区的字节数据，
      For the optional flags argument, see the Unix manual.
      对于这些设置的参数，可以查看Unix手册。
      When no data is available, block untilat least one byte is available or until the remote end is closed.
      当缓冲区没有数据可取时，recv会一直处于阻塞状态，直到缓冲区至少有一个字节数据可取，或者远程端关闭。
      When the remote end is closed and all data is read, return the empty string.
      关闭远程端并读取所有数据后，返回空字符串。
      ```

   2. 高大上版解决粘包方式（自定制包头）

      ![1565925166986](C:\Users\Administrator\Desktop\assets\1565925166986.png)

      我们要制作固定的报头

      你现在有两段不固定长度的bytes类型,我们要固定的报头,所以

      1. 你获取不固定报头的长度

      2. 利用struct模块将不固定的长度转化成固定的字节数 4个字节

      3. 先发4个字节,在发报头数据,在发总数据

         ![1565925587222](C:\Users\Administrator\Desktop\assets\1565925587222.png)

   3. 基于UDP协议的socket通信

   4. socketserver

4. 今日总结

   选课系统讲解:

   ![1565946401485](C:\Users\Administrator\Desktop\assets\1565946401485.png)

   

5. 明日预习