import os
import sys
BATH_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BATH_DIR)
from core import src
from core.src import Course
from core.src import Student
if __name__ == '__main__':
    src.main()
